package org.buczko.pinwheel_04;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.buczko.pinwheel_04.AudioRecorder.AudioAdapter;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioRecord;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioSpeed;
import org.buczko.pinwheel_04.ulit.ClassicListener;
import org.buczko.pinwheel_04.ulit.GraphView;
import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.SpeedRecord;


public class TestAudioActivity extends Activity implements ClassicListener, View.OnClickListener, PinWheelListener {

    private final int BLOCK_SIZE = 16384;
    private AudioAdapter mAudioAdapter;
    private PW_AudioRecord mPWAudioRecord;
    GraphView mGraphView;
    Button mPauseButn;
    boolean mPaused;
    PW_AudioSpeed mAudioSpeedThread;
    SpeedRecord mSpeedRecord;

    //========= Runables =============================
    private OnDone mOnDone;
    private OnSpeedResult mOnSpeedResult;
    //================================================

    private TextView mSpeedTxt;
    private TextView mIntervalTxt;
    private TextView mPeaksTxt;

    private Object mLock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Parameters.setDebug(true);
        mOnSpeedResult = new OnSpeedResult();
        mOnDone = new OnDone();

        mAudioAdapter = new AudioAdapter(this);
        mAudioAdapter.start();

        mPWAudioRecord = new PW_AudioRecord();

        mSpeedRecord = new SpeedRecord(Parameters.getSpeedRecordLenght());
        mAudioSpeedThread = new PW_AudioSpeed(mPWAudioRecord, mSpeedRecord, 100);
        mAudioSpeedThread.setmListener(this);
        mAudioSpeedThread.setStatusWorking(true);
        Thread mPinWheelAudioSpeedThread = new Thread(mAudioSpeedThread);
        mPinWheelAudioSpeedThread.start();

        mGraphView = (GraphView)findViewById(R.id.WindGraph);
        setAudioGrap();
        mPaused = false;
        mPauseButn = (Button)findViewById(R.id.Pause);
        mPauseButn.setOnClickListener(this);
        ((Button)findViewById(R.id.plus_H)).setOnClickListener(this);
        ((Button)findViewById(R.id.minus_H)).setOnClickListener(this);
        ((Button)findViewById(R.id.plus_interval)).setOnClickListener(this);
        ((Button)findViewById(R.id.minus_interval)).setOnClickListener(this);
        ((Button)findViewById(R.id.plus_minGroup)).setOnClickListener(this);
        ((Button)findViewById(R.id.minus_minGropu)).setOnClickListener(this);
        ((TextView)findViewById(R.id.hText)).setText("" + Parameters.getmAudioFactor_H());
        ((TextView)findViewById(R.id.textInterval)).setText("" + Parameters.getInterval());
        ((TextView)findViewById(R.id.textMinGroup)).setText("" + Parameters.getMinPeaksInGroup());

        mSpeedTxt = (TextView)findViewById(R.id.speed);
        mIntervalTxt = (TextView)findViewById(R.id.timeInterval);
        mPeaksTxt = (TextView)findViewById(R.id.peaks);

    }


    private void setAudioGrap(){
        mGraphView.setMarkersArray(mPWAudioRecord.getDebug_peaks());
        mGraphView.setGraph_byref(mPWAudioRecord.getmFloatX());
        mGraphView.mRysikGraph = false;
        mGraphView.mOnlyPositiveGraph = false;
        mGraphView.setShadow(0,0);
        mGraphView.setMarkers(100);
    }

    @Override
    public void onDone() {
        synchronized (mAudioSpeedThread) {
            while(mPWAudioRecord.isFreshData()) {
                try {
                    mAudioSpeedThread.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mAudioAdapter.copyBuffer(mPWAudioRecord.getX());
            mPWAudioRecord.setFreshData(true);
            mAudioSpeedThread.notify();
           //Log.d("COPY ", "====> " + mAudioSpeedThread.mRunCounter);
        };
    }

    private class OnDone implements Runnable{

        @Override
        public void run() {

        }
    }

    @Override
    public void onClick(View v) {
        float h;
        int interval;
        int minGroup;
        switch (v.getId()) {
            case R.id.Pause:
                if (mPaused) {
                    unpauseAudio();
                } else {
                    pauseAudio();
                }
                break;
            case R.id.minus_H:
                h = Parameters.getmAudioFactor_H();
                h -= 0.2f;
                Parameters.setmAudioFactor_H(h);
                ((TextView)findViewById(R.id.hText)).setText("" + Parameters.getmAudioFactor_H());
                if(mPaused){
                    runAudioSpeedOnce();
                }
                break;
           case R.id.plus_H:
                h = Parameters.getmAudioFactor_H();
                h += 0.2f;
                Parameters.setmAudioFactor_H(h);
                ((TextView)findViewById(R.id.hText)).setText("" + Parameters.getmAudioFactor_H());
                if(mPaused){
                    runAudioSpeedOnce();
                }
                break;
            case R.id.minus_interval:
               interval = Parameters.getInterval();
               interval -= 1;
               Parameters.setInterval(interval);
               ((TextView)findViewById(R.id.textInterval)).setText("" + Parameters.getInterval());
               if(mPaused){
                   runAudioSpeedOnce();
               }
               break;
            case R.id.plus_interval:
                interval = Parameters.getInterval();
                interval += 1;
                Parameters.setInterval(interval);
                ((TextView)findViewById(R.id.textInterval)).setText("" + Parameters.getInterval());
                if(mPaused){
                    runAudioSpeedOnce();
                }
                break;
            case R.id.plus_minGroup:
                minGroup = Parameters.getMinPeaksInGroup();
                minGroup += 1;
                Parameters.setMinPeaksInGroup(minGroup);
                ((TextView)findViewById(R.id.textMinGroup)).setText("" + Parameters.getMinPeaksInGroup());
                if(mPaused){
                    runAudioSpeedOnce();
                }
                break;
            case R.id.minus_minGropu:
                minGroup = Parameters.getMinPeaksInGroup();
                minGroup -= 1;
                Parameters.setMinPeaksInGroup(minGroup);
                ((TextView)findViewById(R.id.textMinGroup)).setText("" + Parameters.getMinPeaksInGroup());
                if(mPaused){
                    runAudioSpeedOnce();
                }
                break;
        }
    }

    private void runAudioSpeedOnce(){
        synchronized (mAudioSpeedThread) {
            mPWAudioRecord.setFreshData(true);
            mAudioSpeedThread.notify();
        }
    }

    //=====================on PinWheel listener ====================================================

    @Override
    public void onMagneticSpeedResult() {

    }

    @Override
    public void onAudioSpeedResult() {
        runOnUiThread(mOnSpeedResult);
    }

    private class OnSpeedResult implements Runnable {

        @Override
        public void run() {
            //Log.d("OnSpeedResult", "OnSpeedResult");
            mSpeedTxt.setText("" + mSpeedRecord.getCurrentSpeed()+ " HZ");
            mPWAudioRecord.fillFloatX(150);
            mGraphView.setMarkersArray(mPWAudioRecord.getMedianPeakInt());
            mGraphView.setMarkersArray2(mPWAudioRecord.getJoinedPeaksInt());
            mGraphView.invalidate();
        }
    }

    public void pauseAudio(){
        mAudioAdapter.stop();
        //mPinWheel.stop();
        mPaused = true;
        mPauseButn.setText("START");
    }

    public void unpauseAudio(){
        mAudioAdapter.start();
        //mPinWheel.start();
        mPaused = false;
        mPauseButn.setText("PAUSE");
    }


    @Override
    protected void onPause() {
        super.onPause();
        mAudioSpeedThread.setStatusWorking(false);
        //mAudioAdapter.stop();
        if (!mPaused) {
            pauseAudio();
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPaused) {
            unpauseAudio();
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        //mAudioAdapter.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //mAudioAdapter.quit();

    }
}
