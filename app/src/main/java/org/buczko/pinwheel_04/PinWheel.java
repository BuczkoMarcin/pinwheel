package org.buczko.pinwheel_04;

import android.util.Log;

import org.buczko.pinwheel_04.AudioRecorder.AudioAdapter;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioRecord;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioSpeed;
import org.buczko.pinwheel_04.MagneticRecorder.PW_MagneticRecord;
import org.buczko.pinwheel_04.MagneticRecorder.PW_MagneticSpeed;
import org.buczko.pinwheel_04.ulit.ClassicListener;
import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.SpeedRecord;

/**
 * Created by marcin on 16.05.15.
 */
public class PinWheel implements ClassicListener {

    private PW_MagneticSpeed mPinWheelMagneticSpeed;
    private PW_AudioSpeed mPinWheelAudioSpeed;
    private AudioAdapter mAudioAdapter;

    private PW_AudioRecord mPinWheelAudioRecord;

    private Thread mPinWheelMagneticSpeedThread;
    private Thread mPinWheelAudioSpeedThread;

    private Parameters.PinwheelKind mPinWheelKind;
    private boolean working;


    public PinWheel(PW_MagneticRecord PWMagneticRecord, PW_AudioRecord PWAudioRecord, SpeedRecord speedRecord, int delay) {
        mPinWheelAudioRecord = PWAudioRecord;
        mPinWheelMagneticSpeed = new PW_MagneticSpeed(PWMagneticRecord, speedRecord, delay);
        mPinWheelMagneticSpeedThread = new Thread(mPinWheelMagneticSpeed);
        mPinWheelAudioSpeed = new PW_AudioSpeed(PWAudioRecord,speedRecord,delay);
        mPinWheelAudioSpeedThread = new Thread(mPinWheelAudioSpeed);
        mAudioAdapter = new AudioAdapter(this);
        //mAudioAdapter.start();
        working = false;
        setMagnetic();
    }

    public Parameters.PinwheelKind getPinWheelKind(){
        return  mPinWheelKind;
    }

    public boolean isMagnetic(){
        return mPinWheelKind== Parameters.PinwheelKind.MAGNETIC;
    }

    public boolean isAudio(){
        return mPinWheelKind== Parameters.PinwheelKind.AUDIO;
    }

    public boolean isNon(){
        return mPinWheelKind== Parameters.PinwheelKind.NON;
    }

    public void setMagnetic(){
        if(working) {
            stopAudioThread();
            startMagneticThread();
        };
        mPinWheelKind = Parameters.PinwheelKind.MAGNETIC;

    }

    public void setAudio(){
        if(working) {
            stopMagneticThread();
            startAudioThread();
        };
        mPinWheelKind = Parameters.PinwheelKind.AUDIO;
    }

    public void setNon(){
        stop();
        mPinWheelKind = Parameters.PinwheelKind.NON;
    }

    public void setListener(PinWheelListener listener){
        mPinWheelMagneticSpeed.setmListener(listener);
        mPinWheelAudioSpeed.setmListener(listener);
    }

    public void start(){
        if(mPinWheelKind == Parameters.PinwheelKind.MAGNETIC) {
            startMagneticThread();
            working = true;
        }else if(mPinWheelKind == Parameters.PinwheelKind.AUDIO){
            startAudioThread();
            working = true;
        }
    }

    public void stop(){
        d("stop");
        stopAudioThread();
        stopMagneticThread();
        working = false;
    }

    public void setAverageSpeed(boolean isAverage){
        mPinWheelMagneticSpeed.setStatusAverage(isAverage);
        mPinWheelAudioSpeed.setStatusAverage(isAverage);
    }

    private void startAudioThread(){
        mPinWheelAudioSpeed.setStatusWorking(true);
        if (!mPinWheelAudioSpeedThread.isAlive()) {
            mPinWheelAudioSpeedThread = new Thread(mPinWheelAudioSpeed);
            mPinWheelAudioSpeedThread.start();
        }
        mAudioAdapter.start();
    }

    private void startMagneticThread(){
        mPinWheelMagneticSpeed.setStatusWorking(true);
        if (!mPinWheelMagneticSpeedThread.isAlive()) {
            mPinWheelMagneticSpeedThread = new Thread(mPinWheelMagneticSpeed);
            mPinWheelMagneticSpeedThread.start();
        }
    }

    private void stopAudioThread(){
        mPinWheelAudioSpeed.setStatusWorking(false);
        mAudioAdapter.stop();
    }

    private void stopMagneticThread(){
        d("set magnetic to stop");
        mPinWheelMagneticSpeed.setStatusWorking(false);
    }

    // Audio Adapter Odczytał dane z mikrofonu
    @Override
    public void onDone() {
        d("Audio data ready");
        synchronized (mPinWheelAudioSpeed) {
            while(mPinWheelAudioRecord.isFreshData()) {
                try {
                    d("wait");
                    mPinWheelAudioSpeed.wait();
                    d("wakeup");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mAudioAdapter.copyBuffer(mPinWheelAudioRecord.getX());
            d("Audio data copied");
            mPinWheelAudioRecord.setFreshData(true);
            mPinWheelAudioSpeed.notify();
        };
    }

    protected void d(String msg){
        if(Parameters.isDebug())
            Log.d("PinWheel", "===> " + msg);
    }
}
