package org.buczko.pinwheel_04.MagneticRecorder;

import android.hardware.SensorEvent;
import android.os.Parcel;

import org.buczko.pinwheel_04.ulit.PW_Record;

import java.util.Arrays;

/**
 * Created by marcinb on 2015-05-16.
 */
public class PW_MagneticRecord  extends PW_Record implements android.os.Parcelable {
    private int       mStart_i; //poczatkowy indeks obszaru obliczen
    private int       mStop_i;  // koncowy indeks obszaruobliczen

    public PW_MagneticRecord(int vtableLength) {
        super(vtableLength);
        mStart_i = 0;
        mStop_i = 0;
    };

    public int getStop_i() {
        return mStop_i;
    }

    public void setStop_i(int mStop_i) {
        this.mStop_i = mStop_i;
    }

    public int getStart_i() {
        return mStart_i;
    }

    public void setStart_i(int mStart_i) {
        this.mStart_i = mStart_i;
    }

    @Override
    public String toString() {
        return "PW_MagneticRecord: " + super.toString();
    }

    @Override
    public void reset(){
        super.reset();
        mStart_i = 0;
        mStop_i = 0;
    }

    //=============== Parcelable ===================================================================

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mStart_i);
        dest.writeInt(this.mStop_i);
        dest.writeDoubleArray(this.mX);
        dest.writeLongArray(this.mT);
        dest.writeFloatArray(this.mFloatX);
        dest.writeInt(this.mTableLength);
        dest.writeInt(this.mNextIndex);
        dest.writeIntArray(this.debug_peaks);
        dest.writeInt(this.debug_peaks_index);
    }

    public PW_MagneticRecord(Parcel in) {
        this.mStart_i = in.readInt();
        this.mStop_i = in.readInt();
        this.mX = in.createDoubleArray();
        this.mT = in.createLongArray();
        this.mFloatX = in.createFloatArray();
        this.mTableLength = in.readInt();
        this.mNextIndex = in.readInt();
        this.debug_peaks = in.createIntArray();
        this.debug_peaks_index = in.readInt();
    }

    public static final Creator<PW_MagneticRecord> CREATOR = new Creator<PW_MagneticRecord>() {
        public PW_MagneticRecord createFromParcel(Parcel source) {
            return new PW_MagneticRecord(source);
        }

        public PW_MagneticRecord[] newArray(int size) {
            return new PW_MagneticRecord[size];
        }
    };
}
