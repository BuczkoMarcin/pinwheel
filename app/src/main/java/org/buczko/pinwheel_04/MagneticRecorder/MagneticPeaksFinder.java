package org.buczko.pinwheel_04.MagneticRecorder;

import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.PeaksFinder;

/**
 * Created by marcin on 16.05.15.
 */
public class MagneticPeaksFinder extends PeaksFinder {

    // ==== referancja do danych z sensorow
    private double[] m_MagneticX;   // odczyty z sensora magnetycznego wartosci pola
    private long m_MagneticT[];     // czas w nanosekundach dla kazdego pomiaru
    private int m_maxindex;                 // wielkosc tablicy m_MagneticX i m_MagneticT


    public MagneticPeaksFinder(PW_MagneticRecord PWMagneticRecord) {
        super(PWMagneticRecord);
        m_MagneticX = PWMagneticRecord.getX();
        m_MagneticT = PWMagneticRecord.getT();
        m_maxindex = PWMagneticRecord.getmTableLength();
        debug_peaks = PWMagneticRecord.getDebug_peaks();
        m_T = new long[m_maxindex];
        m_X = new double[m_maxindex];
        m_X_lenght = 0;

        Parameters.setmMagneticPeaksFinder(this);
        mFactor_H = Parameters.getmMagneticFactor_H();
        mValid_H = Parameters.getmMagneticValid_H();
    }

    private void mapXT(int start_index, int stop_index){
        int j = 0;
        if(stop_index < start_index){
            for (int i = start_index; i < m_maxindex; i++, j++) {
                m_X[j] = m_MagneticX[i];
                m_T[j] = m_MagneticT[i];
            }
            for (int i = 0; i <= stop_index && j< m_maxindex ; i++, j++) {
                m_X[j] = m_MagneticX[i];
                m_T[j] = m_MagneticT[i];
            }
        }else {
            for (int i = start_index; i <= stop_index; i++, j++) {
                m_X[j] = m_MagneticX[i];
                m_T[j] = m_MagneticT[i];
            }
        }
        m_X_lenght = j;
    }
  /*
   * stop_index    koncowy index w probkach m_MAgneticX od ktorego zaczynamy liczyc
   * start_index   poczatkowy -  powinien byc last_index+1 (chyba, ze liczymy od poczatku)
   */

    public int findPeaks(int start_index, int stop_index){
        mapXT(start_index,stop_index);
        return super.findPeaks(start_index,stop_index,m_maxindex);
    }

    public void reset(){
        //valid_time = false;
        //last_index = m_index;
        //last_index = 0;
        //old_peak_time = 0;
        mPWRecord.setDebug_peaks(debug_peaks, 0);
        last_min_present = false;
        last_max_present = false;
    }







}
