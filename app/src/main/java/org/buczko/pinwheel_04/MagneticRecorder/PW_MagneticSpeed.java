package org.buczko.pinwheel_04.MagneticRecorder;

import org.buczko.pinwheel_04.ulit.PWSpeedThread;
import org.buczko.pinwheel_04.ulit.SpeedRecord;

/**
 * Created by marcin on 16.05.15.
 */
public class PW_MagneticSpeed extends PWSpeedThread implements Runnable {

    MagneticPeaksFinder magneticPeaksFinder;
    private double m_arcsin[] = new double[101];

    public PW_MagneticSpeed(PW_MagneticRecord PWMagneticRecord, SpeedRecord speedRecord, int delay) {
        super(PWMagneticRecord, speedRecord,delay);
        setArcsinTable();
        magneticPeaksFinder = new MagneticPeaksFinder(PWMagneticRecord);
        magneticPeaksFinder.setDebug(true);

    }

    private void setArcsinTable(){
        for(int i = 0; i <= 100; i++){
            m_arcsin[i] = Math.asin(((i/100f)*2-1)/3.14159f + 0.5f);
        }
    }

    private float ArcSinTable(double X){
        int i = (int)X;
        if(i<0) i=0;
        else
        if(i >= m_arcsin.length) i = m_arcsin.length - 1;
        return (float)m_arcsin[i];
    }

    int current_peak_index;
    long current_peak_time;
    int m_index;
    private int start_index;
    private int stop_index;
    @Override
    protected float countPinwheelSpeed() {

        long old_peak_time = magneticPeaksFinder.getCurrent_peak_time();
        double old_peak_value = magneticPeaksFinder.getCurrent_peak_value();

        float peaks = magneticPeaksFinder.findPeaks(start_index, stop_index);
        long time_interval = 0;

        //Log.d("countPinwheelSpeed()","old_peak_time: "+ old_peak_time + ", old_peak_value: " + old_peak_value + ", current_peak_time: " +current_peak_time + ", current_peak_value: " +current_peak_value + ", peaks:" + peaks);
        if (old_peak_time > 0)
            peaks++;

        if (peaks == 0)
            return 0f;
        else if(peaks == 1){
            if(!magneticPeaksFinder.isLast_max_present() || !magneticPeaksFinder.isLast_min_present())
                return 0f;
            long peak_time;
            double peak_value;
            if(old_peak_time == 0){
                peak_time = magneticPeaksFinder.getCurrent_peak_time();;
                peak_value = magneticPeaksFinder.getCurrent_peak_value();
            }else{
                peak_time = old_peak_time;
                peak_value = old_peak_value;
            }

            if(magneticPeaksFinder.getLast_max_value() == peak_value){
                peaks = 1.0f - ArcSinTable(((float) (magneticPeaksFinder.getLast_X_value() - magneticPeaksFinder.getLast_min_value())) / ((float) (magneticPeaksFinder.getLast_max_value() - magneticPeaksFinder.getLast_min_value())));
            }else{
                peaks = ArcSinTable(((float) (magneticPeaksFinder.getLast_X_value() - magneticPeaksFinder.getLast_min_value())) / ((float) (magneticPeaksFinder.getLast_max_value() - magneticPeaksFinder.getLast_min_value())));
            };
            //Log.d("countPinwheelSpeed()","m_X[m_X_lenght - 1]:" +m_X[m_X_lenght - 1] + ",  last_min_value:" +  last_min_value + ", last_max_value:" + last_max_value + "peaks: " + peaks);
            if(peaks < 0) peaks = 0;
            time_interval = (long) magneticPeaksFinder.getLast_X_value() - peak_time;
        }else if(peaks > 1){
            if(old_peak_time == 0 || peaks > 2) {
                time_interval = magneticPeaksFinder.getCurrent_peak_time() - magneticPeaksFinder.getCurrent_first_peak_time();                    // od pierwszego do ostaniego peaku
                peaks--;
            }else
                time_interval = magneticPeaksFinder.getCurrent_peak_time() - old_peak_time;                              //czas w nanosekundach - od wcześniejszego peaku
            peaks--;
        }

        float speed = (float) peaks / 2.0f / ((float) time_interval / 1000000000);
        if(Float.isNaN(speed))
            speed = 0f;
        //Log.d("countPinwheelSpeed()","time_interval: "+ time_interval + "peaks: " + peaks + "speed: " + speed);
        return   speed;             //obrotow na sekunde
    }

    @Override
    protected void doLoop() {
        m_index = mPWRecord.getmCurrentIndex();
        current_peak_index = magneticPeaksFinder.getCurrent_peak_index();
        current_peak_time = magneticPeaksFinder.getCurrent_peak_time();

        stop_index = (m_index > 0) ? m_index - 1 : m_maxindex - 1;
        start_index = (last_index >= m_maxindex - 1) ? 0 : last_index + 1;
        if (start_index > stop_index && current_peak_index < stop_index)
            current_peak_time = 0;
        else if (start_index < stop_index && current_peak_index <= stop_index && current_peak_index >= start_index)
            current_peak_time = 0;

        //Log.d("run()","start_index:"+start_index+" stop_index:"+stop_index);
        //dodawanie marginesu
        int margin;
        if (start_index < stop_index) {
            margin = m_maxindex + start_index - stop_index - 1;
        } else {
            margin = start_index - stop_index - 1;
        }
        if (current_peak_time > 0 && start_index - current_peak_index < margin) {
            start_index = current_peak_index + 1;
            //Log.d("run()"," current_peak_index:"+ current_peak_index);
        } else
            start_index = start_index - margin;
        if (start_index < 0)
            start_index += m_maxindex;

        ((PW_MagneticRecord)mPWRecord).setStart_i(start_index);
        ((PW_MagneticRecord)mPWRecord).setStop_i(stop_index);
        last_index = stop_index;
    }

    public void reset(int m_index){
        last_index = m_index;
        mSpeedRecord.reset();
        average_speed = 0;
        magneticPeaksFinder.reset();
    }

}
