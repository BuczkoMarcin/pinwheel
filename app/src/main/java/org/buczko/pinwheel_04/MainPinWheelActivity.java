package org.buczko.pinwheel_04;

import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioRecord;
import org.buczko.pinwheel_04.MagneticRecorder.PW_MagneticRecord;
import org.buczko.pinwheel_04.ulit.GraphView;
import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.SpeedRecord;
import java.util.Locale;

/**
 * Created by marcin on 2015-06-02.
 */
public class MainPinWheelActivity extends ActionBarActivity implements SensorEventListener, PinWheelListener{
    //STATES
    private boolean mStatusWorking = false;
    private boolean mStatusAverage = false;
    private boolean mStatusSpeed_graph = true;
    private String  mStatusSpeedScale = Parameters.Hz;

    //UI
    private SensorManager m_SensorManager;
    private GraphView mGraphView;
    private TextView textSpeed_Hz;
    private TextView textStatus;
    Button btnStart;
    private Menu mMenu;

    //PinWheel
    private PW_MagneticRecord mPWMagneticRecord;
    private PW_AudioRecord mPWAudioRecord;
    private SpeedRecord mSpeedRecord;
    private PinWheel mPinWheel;

    //Listeners onSpeedResult
    private OnMagneticSpeedResult mOnMagneticSpeedResult;
    private OnAudioSpeedResult mOnAudioSpeedResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        d("[1] onCreate mStatusAverage:" + mStatusAverage);
        //===== RECORDS & PINWHEEL=========================================
        setFromBundle(savedInstanceState);
        d("[2] onCreate mStatusAverage:" + mStatusAverage);
        //===== Magnetic Sensor =================================
        m_SensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        //===== UI ==============================================
        setContentView(R.layout.activity_main_pinwheel);
        mGraphView = (GraphView)findViewById(R.id.WindGraph);
        textSpeed_Hz = (TextView)findViewById(R.id.textSpeed_Hz);
        textStatus = (TextView)findViewById(R.id.textStatus);
        btnStart = (Button)findViewById(R.id.StartButton);
        Parameters.setDebug(true);
        //======= RUNNABLES ======================================
        mOnMagneticSpeedResult = new OnMagneticSpeedResult();
        mOnAudioSpeedResult = new  OnAudioSpeedResult();
        if(savedInstanceState!=null)
            reincarnate();
        else {
            mPinWheel.setMagnetic();
        }
        showStatus();
    }

    private void reincarnate(){
        if(mStatusWorking)
            start();
        if(mMenu!=null)
            onCreateOptionsMenu(mMenu);
        showSpeed();
    }

    private void setFromBundle(Bundle inst){
        String PinWheelState = "";
        if(inst==null) {
            //ustawiamy dlugosc tablicy z czytywanymi danymiz sensora pola magnetycznego
            mPWMagneticRecord = new PW_MagneticRecord(1024);
            //ustawiamy dlugosc tablicy z odczytamipredkosci i wspolczynnik skali potrzebny dorysowaniua grafu
            mSpeedRecord = new SpeedRecord(Parameters.getSpeedRecordLenght());
            //Instancja dancyh wejsciowych microfonu
            mPWAudioRecord = new PW_AudioRecord();
        }else{
           if(inst.containsKey("mStatusWorking"))     mStatusWorking     = inst.getBoolean("mStatusWorking");
           if(inst.containsKey("mStatusAverage")) mStatusAverage     = inst.getBoolean("mStatusAverage");
           if(inst.containsKey("mStatusSpeed_graph")) mStatusSpeed_graph = inst.getBoolean("mStatusSpeed_graph");
           if(inst.containsKey("mStatusSpeedScale"))  mStatusSpeedScale  = inst.getString("mStatusSpeedScale");
           if(inst.containsKey("mPWMagneticRecord"))  mPWMagneticRecord  = inst.getParcelable("mPWMagneticRecord");
            else mPWMagneticRecord = new PW_MagneticRecord(1024);
           if(inst.containsKey("mPWAudioRecord")){
               mPWAudioRecord     = inst.getParcelable("mPWAudioRecord");
               d("setFromBundle mPWAudioRecord :" + mPWAudioRecord.getJoinedPeaks().toString());
           }else{
               d("setFromBundle No mPWAudioRecord  !!");
               mPWAudioRecord = new PW_AudioRecord();
           }
           if(inst.containsKey("mSpeedRecord"))       mSpeedRecord       = inst.getParcelable("mSpeedRecord");
            else mSpeedRecord = new SpeedRecord(Parameters.getSpeedRecordLenght());
           if(inst.containsKey("PinWheelState"))      PinWheelState      = inst.getString("PinWheelState");
        }
        // ====== PINWHEEL ======================================
        //wskazania na recordy i delay w milisecundach pco jaki czas beda robione obliczeni predkosci
        mPinWheel = new PinWheel(mPWMagneticRecord, mPWAudioRecord, mSpeedRecord, 1000);
        mPinWheel.setListener(this);
        switch(PinWheelState){
            case "Audio"   : mPinWheel.setAudio(); break;
            case "Magnetic": mPinWheel.setMagnetic(); break;
            default: mPinWheel.setNon();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mSpeedRecord!=null) {
            d("MainPinWheelActivity goes to Bundle!");
            String PinWheelState;
            if(mPinWheel.isAudio())
                PinWheelState = "Audio";
            else if(mPinWheel.isMagnetic())
                PinWheelState = "Magnetic";
            else
                PinWheelState = "None";
            d("Saving mStatusAverage: " + mStatusAverage);
            outState.putBoolean("mStatusWorking",mStatusWorking);
            outState.putBoolean("mStatusAverage",mStatusAverage);
            outState.putBoolean("mStatusSpeed_graph",mStatusSpeed_graph);
            outState.putString("mStatusSpeedScale",mStatusSpeedScale);
            outState.putParcelable("mPWMagneticRecord",mPWMagneticRecord );
            outState.putParcelable("mPWAudioRecord", mPWAudioRecord);
            outState.putParcelable("mSpeedRecord", mSpeedRecord);
            outState.putString("PinWheelState",PinWheelState);

        }
        super.onSaveInstanceState(outState);
    }

    //================== START STOP ================================================================

    public void stop(){
        d("Stoping mStatusWorking: " + mStatusWorking);
        mStatusWorking=false;
        mPinWheel.stop();
        btnStart.setText("START");
    }

    public void start(){
        mStatusWorking=true;
        mPinWheel.start();
        btnStart.setText("STOP");
    }

    protected void onResume() {
        super.onResume();
        m_SensorManager.registerListener(this, m_SensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
        showStatus();
    }

    protected void onPause() {
        super.onPause();
        m_SensorManager.unregisterListener(this);
        boolean _mStatusWorking = mStatusWorking;
        stop();
        mStatusWorking = _mStatusWorking; // ze względu na onSaveInstanceState(), ktore nie zgodnie z teoria jest wolane po on pause.
    }

    //======================= MENU =================================================================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem avr_on = mMenu.findItem(R.id.action_AvgOn);
        MenuItem avr_off = mMenu.findItem(R.id.action_AvgOff);
        MenuItem grap_speed = mMenu.findItem(R.id.action_SpeedGraph);
        MenuItem grap_signal = mMenu.findItem(R.id.action_SignalGraph);
        MenuItem audioSignal = mMenu.findItem(R.id.action_AudioOn);
        MenuItem magneticSignal = mMenu.findItem(R.id.action_MagneticOn);
        MenuItem speedScaleHz = mMenu.findItem(R.id.action_HzOn);
        MenuItem speedScaleKm = mMenu.findItem(R.id.action_KmOn);
        MenuItem speedScaleMs = mMenu.findItem(R.id.action_MsOn);

        if(mStatusAverage)
            onSetAverageOn(avr_on);
        else
            onSetAverageOff(avr_off);

        if(mStatusSpeed_graph)
            onSetSpeedGraph(grap_speed);
        else
            onSetSignalGraph(grap_signal);

        if(mPinWheel.isAudio())
            onSetAudioPinWheel(audioSignal);
        else if(mPinWheel.isMagnetic())
            onSetMagneticPinWheel(magneticSignal);

        switch(mStatusSpeedScale){
            case Parameters.Hz :  onSpeedScaleHz(speedScaleHz);break;
            case Parameters.kmh : onSpeedScaleKm(speedScaleKm); break;
            case Parameters.ms :  onSpeedScaleMs(speedScaleMs); break;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.action_AvgOn:{
                onSetAverageOn(item);
                return true;
            }
            case R.id.action_AvgOff:{
                onSetAverageOff(item);
                return true;
            }
            case R.id.action_SpeedGraph:{
                onSetSpeedGraph(item);
                return true;
            }
            case R.id.action_SignalGraph:{
                onSetSignalGraph(item);
                return true;
            }
            case R.id.action_clear:{
                clearData();
                return true;
            }
            case R.id.action_AudioOn:{
                onSetAudioPinWheel(item);
                return true;
            }
            case R.id.action_MagneticOn:{
                onSetMagneticPinWheel(item);
                return true;
            }
            case R.id.action_HzOn:{
                onSpeedScaleHz(item);
                return true;
            }
            case R.id.action_KmOn:{
                onSpeedScaleKm(item);
                return true;
            }
            case R.id.action_MsOn:{
                onSpeedScaleMs(item);
                return true;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    public void clearData(){
        mSpeedRecord.reset();
        mPWMagneticRecord.reset();

        if(mStatusSpeed_graph) {
            setSpeedGraph();
            mGraphView.setRysikIndex(0);
        }else {
            if(mPinWheel.isMagnetic())
                setMagneticGraph();
            else if(mPinWheel.isAudio())
                setAudioGrap();
        }
        mGraphView.invalidate();
    }

    private void onSetAudioPinWheel(MenuItem item){
        MenuItem pinwheelMenuItem = mMenu.findItem(R.id.action_pinwhell);
        if(pinwheelMenuItem !=null)
            pinwheelMenuItem .setTitle("Pinwheel: Audio");
        item.setChecked(true);
        MenuItem hzMenuItem = mMenu.findItem(R.id.action_HzOn);
        if( hzMenuItem!= null)
            onSpeedScaleHz(hzMenuItem);
        if(mPinWheel.isAudio())
            return;
        clearData();
        boolean l_mStatusWorking = mStatusWorking;
        if(mStatusWorking) {
            d("--3-- onSetAudioPinWheel");
            stop();
        }
        mPinWheel.setAudio();
        Parameters.setSpeedRecordGraphFactor(Parameters.getSpeedRecordAudioGraphFactor());
        if(l_mStatusWorking)
            start();
        if(!mStatusSpeed_graph)
            onSetSignalGraph(mMenu.findItem(R.id.action_SignalGraph));
        showStatus();
    }

    private void onSetMagneticPinWheel(MenuItem item){
        MenuItem pinwheelMenuItem = mMenu.findItem(R.id.action_pinwhell);
        if(pinwheelMenuItem !=null)
            pinwheelMenuItem .setTitle("Pinwheel: Magnetic");
        item.setChecked(true);
        if(mPinWheel.isMagnetic())
            return;
        clearData();
        boolean l_mStatusWorking = mStatusWorking;
        if(mStatusWorking) {
            d("--2-- onSetMagneticPinWheel");
            stop();
        }
        mPinWheel.setMagnetic();
        Parameters.setSpeedRecordGraphFactor(Parameters.getSpeedRecordMagneticGraphFactor());
        if(l_mStatusWorking)
            start();
        if(!mStatusSpeed_graph)
            onSetSignalGraph(mMenu.findItem(R.id.action_SignalGraph));
        showStatus();
    }

    private void onSetSpeedGraph(MenuItem item){
        mStatusSpeed_graph = true;
        MenuItem graphMenuItem = mMenu.findItem(R.id.action_graph);
        if(graphMenuItem!=null)
            graphMenuItem.setTitle("Graph: Speed");
        item.setChecked(true);
        setSpeedGraph();
        mGraphView.autoScaleX();
        mGraphView.invalidate();
        showStatus();
    }

    private void onSetSignalGraph(MenuItem item){
        mStatusSpeed_graph = false;
        item.setChecked(true);
        MenuItem graphMenuItem = mMenu.findItem(R.id.action_graph);
        if(graphMenuItem!=null)
            graphMenuItem.setTitle("Graph: Signal");
        if(mPinWheel.isMagnetic()) {
            setMagneticGraph();
        }else if(mPinWheel.isAudio()){
            setAudioGrap();
        }
        mGraphView.autoScaleX();
        mGraphView.invalidate();
        showStatus();
    }

    private void onSetAverageOn(MenuItem item){
        mStatusAverage = true;
        MenuItem averageMenuItem = mMenu.findItem(R.id.action_average);
        if(averageMenuItem!=null)
            averageMenuItem.setTitle("Average: On");
        item.setChecked(true);
        mPinWheel.setAverageSpeed(true);
        mSpeedRecord.calculateAverageSpeed();
        mGraphView.setAvgLine(mSpeedRecord.getmAverage_speed());
        mGraphView.invalidate();
        showStatus();
    }

    private void onSetAverageOff(MenuItem item){
        d("onSetAverageOff mStatusAverage:" + mStatusAverage);
        mStatusAverage = false;
        MenuItem averageMenuItem = mMenu.findItem(R.id.action_average);
        if(averageMenuItem!=null)
            averageMenuItem.setTitle("Average: Off");
        item.setChecked(true);
        mPinWheel.setAverageSpeed(false);
        mGraphView.setAvgLine(0);
        mGraphView.invalidate();
        showStatus();

    }

    private void onSpeedScaleHz(MenuItem item){
        MenuItem averageMenuItem = mMenu.findItem(R.id.action_SpeedScale);
        if(averageMenuItem!=null)
            averageMenuItem.setTitle("Speed: Hz");
        item.setChecked(true);
        mStatusSpeedScale = Parameters.Hz;
        showSpeed();
    }

    private void onSpeedScaleKm(MenuItem item){
        if(!mPinWheel.isMagnetic()) return;
        MenuItem speedMenuItem = mMenu.findItem(R.id.action_SpeedScale);
        if(speedMenuItem!=null)
            speedMenuItem.setTitle("Speed: Km/h");
        item.setChecked(true);
        mStatusSpeedScale = Parameters.kmh;
        showSpeed();
    }

    private void onSpeedScaleMs(MenuItem item){
        if(!mPinWheel.isMagnetic()) return;
        MenuItem speedMenuItem = mMenu.findItem(R.id.action_SpeedScale);
        if(speedMenuItem!=null)
            speedMenuItem.setTitle("Speed: Ms/s");
        item.setChecked(true);
        mStatusSpeedScale = Parameters.ms;
        showSpeed();
    }


    //===================== Start Buttn listener ===================================================
    public void onClick(View v) {

        //v.playSoundEffect(SoundEffectConstants.CLICK);
        switch (v.getId()){
            case R.id.StartButton: {
                onStartButtonClick();
            }break;
        }
        showStatus();
    }

    public void onStartButtonClick(){
        if(mStatusWorking){
            d("--1-- ButtonClik Stop");
            stop();
        }else{
            start();
        }
    }

    //===================== UI =====================================================================

    private void showStatus(){
        if(textStatus==null) return;
        String statstxt = "";
        if(mStatusWorking)
            statstxt += "WORKING";
        else
            statstxt += "STOPED";

        if(mStatusAverage)
            statstxt += "/A";

        if(mStatusSpeed_graph)
            statstxt += "/SPD";
        else
            statstxt += "/SIG";

        if(mPinWheel.isMagnetic())
            statstxt += "/MAGNETIC";
        else if(mPinWheel.isAudio())
            statstxt += "/AUDIO";

        textStatus.setText(statstxt);
    }

    private void showSpeed(){
        float speed = 0;
        if(mStatusAverage) {
            speed = mSpeedRecord.getmAverage_speed();
        }else
            speed = mSpeedRecord.getCurrentSpeed();

        if(speed > 0.1f) {
            switch (mStatusSpeedScale) {
                case Parameters.Hz:
                    break;
                case Parameters.kmh:
                    speed = 2 * speed + 7;
                    break;
                case Parameters.ms:
                    speed = 0.277f * (2 * speed + 7);
                    break;
                default:
                    mStatusSpeedScale = Parameters.Hz;
            }
        }
        if(Float.isNaN(speed))
            speed = 0.001f;
        if(Parameters.getScreenOrientation(this) == Configuration.ORIENTATION_PORTRAIT)
            textSpeed_Hz.setText(String.format(Locale.US,"%.1f", speed) + " "+mStatusSpeedScale);
        else
            textSpeed_Hz.setText(String.format(Locale.US,"%.1f", speed) + "\n"+mStatusSpeedScale);

    }

    //===================== GRAPHS =================================================================

    private void setMagneticGraph(){
        mGraphView.setMarkersArray(mPWMagneticRecord.getDebug_peaks());
        mGraphView.setGraph_byref(mPWMagneticRecord.getmFloatX());
        mGraphView.mRysikGraph = false;
        mGraphView.mOnlyPositiveGraph = false;
        mGraphView.setShadow(mPWMagneticRecord.getStart_i(), mPWMagneticRecord.getStop_i());
        mGraphView.setMarkers(50);
    }

    private void setSpeedGraph(){
        mGraphView.setMarkersArray(null);
        mGraphView.setGraph_byref(mSpeedRecord.getM_out());
        mGraphView.mRysikGraph = true;
        mGraphView.mOnlyPositiveGraph = true;
        mGraphView.setRysikIndex(mSpeedRecord.getCurrentIndex());
        mGraphView.setShadow(0, 0);
        mGraphView.setMarkers(0);
        if(mStatusAverage)
            mGraphView.setAvgLine(mSpeedRecord.getmAverage_line());
    }

    private void setAudioGrap(){
        mGraphView.setMarkersArray(mPWAudioRecord.getMedianPeakInt());
        mGraphView.setMarkersArray2(mPWAudioRecord.getJoinedPeaksInt());
        mGraphView.setGraph_byref(mPWAudioRecord.getmFloatX());
        mGraphView.mRysikGraph = false;
        mGraphView.mOnlyPositiveGraph = false;
        mGraphView.setShadow(0,0);
        mGraphView.setMarkers(100);
    }

    private void showMagneticGraph(){
        mGraphView.setShadow(mPWMagneticRecord.getStart_i(), mPWMagneticRecord.getStop_i());
        mGraphView.setMarkersArray(mPWMagneticRecord.getDebug_peaks());
        mGraphView.invalidate();
    }

    private void showSpeedGraph(){
        if(mStatusAverage)
            mGraphView.setAvgLine(mSpeedRecord.getmAverage_line());
        mGraphView.setRysikIndex(mSpeedRecord.getCurrentIndex());
        mGraphView.invalidate();

    }

    private void showAudioGrap(){
        mPWAudioRecord.fillFloatX(150);
        mGraphView.setMarkersArray(mPWAudioRecord.getMedianPeakInt());
        mGraphView.setMarkersArray2(mPWAudioRecord.getJoinedPeaksInt());
        mGraphView.invalidate();
    }

    //===================== on PinWheel listener ====================================================

    @Override
    public void onMagneticSpeedResult() {
        runOnUiThread(mOnMagneticSpeedResult);
    }

    @Override
    public void onAudioSpeedResult() {
        runOnUiThread(mOnAudioSpeedResult);
    }

    private class OnMagneticSpeedResult implements Runnable {

        @Override
        public void run() {
            showSpeed();
            if(mStatusSpeed_graph)
                showSpeedGraph();
            else
                showMagneticGraph();
        }
    }

    private class OnAudioSpeedResult implements Runnable {

        @Override
        public void run() {
            showSpeed();
            if(mStatusSpeed_graph)
                showSpeedGraph();
            else
                showAudioGrap();
        }
    }

    //============================ Sensor Listener =================================================

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(mStatusWorking && mPinWheel.isMagnetic()) {
            mPWMagneticRecord.add(event);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

   //============================= Utils ===========================================================

    protected void d(String msg){
        if(!Parameters.isDebug())
            return;
         Log.d("PMainPinWheelActivity", "====> " + msg + " <====");
    }


}
