package org.buczko.pinwheel_04.ulit;

import android.hardware.SensorEvent;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by marcin on 2015-05-31.
 */
public class PW_Record  {
    protected double    mX[];   // odczyty z sensora magnetycznego wartosci pola
    protected long      mT[];     // czas w nanosekundach dla kazdego pomiaru
    protected float     mFloatX[];
    protected int       mTableLength;         // wielkosc tablicy m_MagneticX i m_MagneticT
    protected int       mNextIndex;
    protected int       debug_peaks[];
    protected int       debug_peaks_index;

    // na potrzeby parcelable klas dziedziczonych
    protected PW_Record(){

    }

    public PW_Record(int vtableLength) {
        mTableLength = vtableLength;
        mX = new double[mTableLength];
        mFloatX = new float[mTableLength];
        mT = new long[mTableLength];
        debug_peaks = new int[mTableLength];
        debug_peaks_index = 0;
        mNextIndex = 0;
    };

    public void add(double xValue, long tValue){
        mX[mNextIndex] = xValue;
        mFloatX[mNextIndex] = (float)xValue;
        mT[mNextIndex] = tValue;
        mNextIndex = (mNextIndex + 1 == mTableLength) ? 0 : mNextIndex + 1;
    };

    public void add(SensorEvent event){
        mT[mNextIndex] = event.timestamp;
        mX[mNextIndex] = event.values[0];
        mFloatX[mNextIndex] = (float)event.values[0];
        mNextIndex = (mNextIndex + 1 == mTableLength) ? 0 : mNextIndex + 1;
    }

    public int getmTableLength() {
        return mTableLength;
    }

    public int getDebug_peaks_index() {
        return debug_peaks_index;
    }

    public int getmNextIndex() {
        return mNextIndex;
    }

    public int getmCurrentIndex() {
        if(mNextIndex == 0) return 0;
        return mNextIndex-1;
    }

    public double[] getX() {
        return mX;
    }

    public long[] getT() {
        return mT;
    }

    public int[] getDebug_peaks() {
        return debug_peaks;
    }

    public boolean setDebug_peaks(int[] debug_peaks, int debug_peaks_index) {
        if( debug_peaks.length != mTableLength || debug_peaks_index >= mTableLength)
            return false;
        this.debug_peaks = debug_peaks;
        this.debug_peaks_index = debug_peaks_index;
        return true;
    }

    public float[] getmFloatX() {
        return mFloatX;
    }

    public double getX(int index){
        return mX[index];
    }


    @Override
    public String toString() {
        return "PW_Record{" +
                "mX=" + Arrays.toString(mX) +
                ", mT=" + Arrays.toString(mT) +
                ", mFloatX=" + Arrays.toString(mFloatX) +
                ", mTableLength=" + mTableLength +
                ", mNextIndex=" + mNextIndex +
                '}';
    }

    public void reset(){
        debug_peaks_index = 0;
        mNextIndex = 0;
        for(int i=0; i< mTableLength; i++){
            mX[i] = 0;
            mFloatX[i] = 0;
            debug_peaks[i] = 0;
        }

    }


}
