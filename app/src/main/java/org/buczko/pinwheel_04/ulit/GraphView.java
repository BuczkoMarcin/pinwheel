package org.buczko.pinwheel_04.ulit;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import org.buczko.pinwheel_04.R;

import static org.buczko.pinwheel_04.R.color.shadow;

/**
 * Created by marcin on 2015-03-16.
 */
public class GraphView extends View{

    //==== parameters ==============
    private int mmarker_height = 10;
    float mRysikSize = 10;

    //==== Public =================

    public boolean mRysikGraph = false;
    public boolean mOnlyPositiveGraph = false;
    private int mRysikIndex = 0;

    //=============================
    private int mmaxindex = 0;
    Paint paint_wykresX;
    Paint paint_wykresY;
    Paint paint_wykresZ;
    Paint paint_os;
    Paint paint_marker;
    Paint paint_shadow;
    Paint paint_rysik;
    private float mX[];
    private float mY[];
    private float mZ[];
    private int mmarker = 0;
    private int[] mmarkersArray;
    int mmarkersArrayLenght = 0;
    private int[] mmarkersArray2;
    int mmarkersArrayLenght2 = 0;
    int shadowBg_Start=0;
    int shadowBg_Stop=0;
    private int mCurrentMaxI;
    private int mheight;
    private int mwidth;
    private ScaleGestureDetector mScaleDetector;
    private GestureDetector mDoubleTapDetector;
    private float mScaleFactorX;
    private float mScaleFactorY;
    private static int NONE = 0;
    private static int DRAG = 1;
    private static int ZOOM = 2;
    private boolean dragged=false;
    private boolean scaled=false;
    private boolean autoscaledX = false;
    private boolean doubletapped=false;
    private int mode;
    private float startX = 0f;
    private float startY = 0f;
    private float translateX = 0f;
    private float translateY = 0f;
    private float mFocuseX = 0f;
    private float mFocuseY = 0f;

    private float mAvgLine = 0;



    Context mcontext;

    public void setparams(Context context){
        mcontext = context;
        mX = null;
        mY = null;
        mZ = null;
        mCurrentMaxI = mmaxindex;
        paint_wykresX = new Paint();
        paint_wykresX.setColor(Color.RED);
        paint_wykresX.setStyle(Paint.Style.STROKE);
        paint_wykresX.setAntiAlias(true);
        paint_wykresX.setStrokeWidth(1f);
        paint_wykresY = new Paint(paint_wykresX);
        paint_wykresY.setColor(Color.BLUE);
        paint_wykresZ = new Paint(paint_wykresX);
        paint_wykresZ.setColor(Color.GREEN);
        paint_os      = new Paint(paint_wykresX);
        paint_os.setStrokeWidth(1f);
        paint_os.setColor(Color.GRAY);
        paint_marker  = new Paint(paint_wykresX);
        paint_marker.setStrokeWidth(1f);
        paint_marker.setColor(Color.BLACK);
        paint_shadow = new Paint();
        paint_shadow.setStyle(Paint.Style.FILL);
        paint_shadow.setColor(R.color.shadow);
        paint_rysik = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint_rysik.setStrokeWidth(1f);
        paint_rysik.setColor(Color.RED);
        paint_rysik.setStyle(Paint.Style.FILL_AND_STROKE);
        paint_rysik.setAntiAlias(true);


        mheight = 0;
        mwidth = 0;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        mDoubleTapDetector = new GestureDetector(context, new DoubleTapListener());
        mScaleFactorX = 1.f;
        mScaleFactorY = 1.f;
    }

    public GraphView(Context context, AttributeSet attributeSet){
        super(context,attributeSet);
        setparams(context);
    }

    public GraphView(Context context) {
        super(context);
        setparams(context);

    }

    public void autoScaleX(){
        if(mwidth == 0 || mmaxindex <= 0)
            return;
        if(mRysikGraph) {
            mScaleFactorX = (float) mwidth / (mmaxindex+3);
            translateX = mRysikSize;
        }else{
            mScaleFactorX = (float) mwidth / (float)mmaxindex;
            translateX = 0;
        };
        if(mScaleFactorX > 15.0f) mScaleFactorX = 15.0f;
        if(mScaleFactorX < 0.1f) mScaleFactorX = 0.1f;
        setAutoTranslateY();
        autoscaledX = true;

    }

    public void setGraph_byref(float X[]){
        mmaxindex = X.length-1;
        mX =  X;
        mY =  null;
        mZ =  null;
    }

    public void setGraph(float X[], float Y[], float Z[]){
        mmaxindex = X.length;
        if (Y.length > mmaxindex) mmaxindex = Y.length;
        if (Z.length > mmaxindex) mmaxindex = Z.length;
        mX =  new float[mmaxindex+1];
        mY =  new float[mmaxindex+1];
        mZ =  new float[mmaxindex+1];
        System.arraycopy(X,0,mX,0,X.length);
        System.arraycopy(Y,0,mY,0,Y.length);
        System.arraycopy(Z,0,mZ,0,Z.length);
    }

    public void setMarkers(int marker){
        mmarker = marker;
    }

    public void setMarkersArray(int[] markers){
        if(markers!=null && markers.length == 0) {
            mmarkersArray = null;
            mmarkersArrayLenght = 0;
            return;
        }
        mmarkersArray = markers;
        if(markers!=null)mmarkersArrayLenght = markers.length;
        else mmarkersArrayLenght = 0;
    }

    public void setMarkersArray2(int[] markers){
        if(markers!=null && markers.length == 0) {
            mmarkersArray2 = null;
            mmarkersArrayLenght2 = 0;
            return;
        }
        mmarkersArray2 = markers;
        if(markers!=null)mmarkersArrayLenght2 = markers.length;
        else mmarkersArrayLenght2 = 0;
    }

    public void setRysikIndex(int index){
       //if(index < 0) mRysikIndex = mmaxindex -1;
        //else if(index >= mmaxindex) mRysikIndex = 0;
        mRysikIndex = index;
    }

    public void setShadow(int startX, int stopX){
        shadowBg_Start = startX;
        shadowBg_Stop = stopX;
    }

    public void setAvgLine(float lineY){
        mAvgLine = lineY;
    }

    protected void drawShadow(Canvas canvas){
        if(shadowBg_Start == shadowBg_Stop)
            return;
        if(shadowBg_Start < shadowBg_Stop){
            canvas.drawRect(shadowBg_Start * mScaleFactorX + translateX, mheight, shadowBg_Stop * mScaleFactorX + translateX, 0, paint_shadow);
        }else{
            canvas.drawRect(translateX > 0 ? translateX : 0, mheight, shadowBg_Stop * mScaleFactorX + translateX, 0, paint_shadow);
            canvas.drawRect(shadowBg_Start * mScaleFactorX + translateX, mheight, (mCurrentMaxI -1)* mScaleFactorX + translateX, 0, paint_shadow);
        }
    }

    protected void drawRysik(Canvas canvas,float Y){
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(translateX,Y);
        path.lineTo(translateX-mRysikSize,Y-mRysikSize);
        path.lineTo(translateX-mRysikSize,Y+mRysikSize);
        path.lineTo(translateX,Y);
        path.close();
        canvas.drawPath(path, paint_rysik);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
         if(autoscaledX==false)
             autoScaleX();
         setmCurrentMaxI();
         drawShadow(canvas);
         canvas.drawLine(0,translateY,mwidth,translateY,paint_os);
         //canvas.drawLine(shadowBg_Start* mScaleFactorX + translateX,0,shadowBg_Start* mScaleFactorX + translateX,mheight,paint_marker);
         //canvas.drawLine(shadowBg_Stop* mScaleFactorX + translateX,0,shadowBg_Stop* mScaleFactorX + translateX,mheight,paint_marker);
         if(mAvgLine!=0)
             canvas.drawLine(0,mAvgLine*mScaleFactorY + translateY,mwidth,mAvgLine*mScaleFactorY + translateY,paint_os);

         int   X = translateX > 0 ? 0 : -(int)(translateX/mScaleFactorX);
         float Y1;
         float Y2;
         for(int i = X,k = 0,k2=0; i< mCurrentMaxI; i++,X++) {
             if(mX != null){
                 if(mRysikGraph && mRysikIndex <= mmaxindex){
                     if(i <= mRysikIndex) {
                         Y2 =   mX[mRysikIndex - i];
                         if(i==0) {
                             Y1 = Y2;
                             drawRysik(canvas,Y1* mScaleFactorY + translateY);
                         }else {
                             Y1 = mX[mRysikIndex - i + 1];
                         }
                     }else{
                         if(i == mRysikIndex + 1){
                             canvas.drawLine(X * mScaleFactorX + translateX, mX[0]* mScaleFactorY + translateY , (X + 1)*mScaleFactorX + translateX, mX[mmaxindex]* mScaleFactorY + translateY, paint_wykresX);
                             X++;
                         }
                         Y2 =  mX[mmaxindex + mRysikIndex - i];
                         Y1 =  mX[mmaxindex + mRysikIndex - i + 1];
                     }

                 }else{
                     Y1 =  mX[i];
                     Y2 =  mX[i + 1];
                 }
                 Y1 = Y1 * mScaleFactorY + translateY;
                 Y2 = Y2 * mScaleFactorY + translateY;

                 canvas.drawLine(X * mScaleFactorX + translateX, Y1 , (X + 1)*mScaleFactorX + translateX, Y2, paint_wykresX);

             }
            //if(mY != null) canvas.drawLine(i * mScaleFactorX + translateX, mY[i] * mScaleFactorY + translateY, (i + 1)*mScaleFactorX + translateX, mY[i + 1] * mScaleFactorY + translateY, paint_wykresY);
            //if(mZ != null) canvas.drawLine(i * mScaleFactorX + translateX, mZ[i] * mScaleFactorY + translateY, (i + 1)*mScaleFactorX + translateX, mZ[i + 1] * mScaleFactorY + translateY, paint_wykresZ);
            if(mmarker!= 0 && i%mmarker==0)
                 canvas.drawLine(i*mScaleFactorX + translateX,translateY,i*mScaleFactorX + translateX,translateY+mmarker_height*mScaleFactorY,paint_marker);


            if(mmarkersArray!=null && k==0 && mmarkersArray[k] < i ){
                while(k< mmarkersArrayLenght && mmarkersArray[k]<i) k++;
            }
            if(mmarkersArray!=null && k < mmarkersArrayLenght && mmarkersArray[k] == i) {
                canvas.drawLine(i * mScaleFactorX + translateX, 0, i * mScaleFactorX + translateX, mheight, paint_os);
                k++;
            };


             if(mmarkersArray2!=null && k2==0 && mmarkersArray2[k2] < i ){
                 while(k2< mmarkersArrayLenght2 && mmarkersArray2[k2]<i) k2++;
             }
             if(mmarkersArray2!=null && k2 < mmarkersArrayLenght2 && mmarkersArray2[k2] == i) {
                 float strokeWidth = paint_marker.getStrokeWidth();
                 paint_marker.setStrokeWidth(2f);
                 canvas.drawLine(i * mScaleFactorX + translateX, 0, i * mScaleFactorX + translateX, mheight, paint_marker);
                 paint_marker.setStrokeWidth(strokeWidth);
                 k2++;
             };


         };

         if(mRysikGraph) {
             if (mRysikIndex == mmaxindex - 1) {
                 canvas.drawLine(X * mScaleFactorX + translateX, mX[0]* mScaleFactorY + translateY , (X + 1)*mScaleFactorX + translateX, mX[mmaxindex]* mScaleFactorY + translateY, paint_wykresX);
             }else if(mRysikIndex == mmaxindex){
                 canvas.drawLine(X * mScaleFactorX + translateX, mX[1]* mScaleFactorY + translateY , (X + 1)*mScaleFactorX + translateX, mX[0]* mScaleFactorY + translateY, paint_wykresX);
             }
         }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mheight = h;
        mwidth = w;
        setAutoTranslateY();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

       switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                mode = DRAG;
                startX = event.getX() - translateX;
                startY = event.getY() - translateY;
                break;
            case MotionEvent.ACTION_MOVE:
                if(mode==DRAG) {
                    if (translateX - (event.getX() - startX) != 0 || translateY - (event.getY() - startY) != 0) {
                        translateX = event.getX() - startX;
                        translateY = event.getY() - startY;
                        dragged = true;
                    } else
                        dragged = false;
                }else if(mode==NONE){
                    mode = DRAG;
                    startX = event.getX() - translateX;
                    startY = event.getY() - translateY;
                }
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                mode = ZOOM;
                break;
            case MotionEvent.ACTION_UP:
                mode = NONE;
                dragged = false;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
        }

       mScaleDetector.onTouchEvent(event);
       doubletapped = false;
       mDoubleTapDetector.onTouchEvent(event);
       if ((mode == DRAG && dragged) || (mode == ZOOM &&  scaled ) || doubletapped) {
            invalidate();
       }
       return true;
    }

    public void setAutoTranslateY(){
        if(mOnlyPositiveGraph)
            translateY = (int)(mheight * 0.9f);
        else
            translateY = mheight/2;
    }

    private void setmCurrentMaxI(){
        mheight = getHeight();
        mwidth  = getWidth();

        int widthX = (int)((mwidth-translateX)/mScaleFactorX);
        if(widthX > 0){
            if(widthX<mmaxindex)
                mCurrentMaxI = widthX;
            else
                mCurrentMaxI = mmaxindex;
        }
    }

    //================================== Scale Listener ===========================
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if(Math.abs(detector.getCurrentSpanX() - detector.getPreviousSpanX()) > Math.abs(detector.getCurrentSpanY() - detector.getPreviousSpanY())){
                float CurrentScaleFactorX = Math.max(0.9f,Math.min(detector.getCurrentSpanX() / detector.getPreviousSpanX(),1.1f));
                float oldSaleFactorX = mScaleFactorX;
                mScaleFactorX *= CurrentScaleFactorX;
                if(mScaleFactorX > 15.0f || mScaleFactorX < 0.1f ){
                    if(mScaleFactorX > 15.0f) mScaleFactorX = 15.0f;
                    if(mScaleFactorX < 0.1f) mScaleFactorX = 0.1f;
                    CurrentScaleFactorX = mScaleFactorX/oldSaleFactorX;
                }
                if(CurrentScaleFactorX == 1){
                    scaled = false;
                    return true;
                }
                else
                    scaled = true;
                mFocuseX = detector.getFocusX();
                translateX = mFocuseX - (mFocuseX - translateX) * CurrentScaleFactorX;
            }else{
                float CurrentScaleFactorY =Math.max(0.9f,Math.min(detector.getCurrentSpanY() / detector.getPreviousSpanY(),1.1f));
                float oldSaleFactorY = mScaleFactorY;
                mScaleFactorY *= CurrentScaleFactorY;
                if(mScaleFactorY > 15.0f || mScaleFactorY < 0.1f ){
                    if(mScaleFactorY > 15.0f) mScaleFactorY = 15.0f;
                    if(mScaleFactorY < 0.1f) mScaleFactorY = 0.1f;
                    CurrentScaleFactorY = mScaleFactorY/oldSaleFactorY;
                }
                if(CurrentScaleFactorY == 1){
                    scaled = false;
                    return true;
                }
                else
                    scaled = true;
                mFocuseY = detector.getFocusY();
                translateY = mFocuseY - (mFocuseY - translateY) * CurrentScaleFactorY;
            };
            return true;
        }
    }

    private class DoubleTapListener extends GestureDetector.SimpleOnGestureListener{

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        setAutoTranslateY();
        translateX = 0;
        mScaleFactorY = 1f;
        //mScaleFactorX = 1f;
        autoScaleX();
        startX = translateX;
        startY = translateY;
        mode = NONE;
        doubletapped = true;
        //Log.d("onDoubleTap","translateY: " + translateY);
        return super.onDoubleTap(e);
    }
    }

}
