package org.buczko.pinwheel_04.ulit;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.buczko.pinwheel_04.ulit.ClassicListener;

/**
 * Created by marcin on 2015-05-19.
 */
public abstract class ClassicThread {

    private Looper mLooper = null;
    private Handler mHandler = null;
    private boolean mStop = false;
    private boolean mStopped = false;
    private boolean mWorking = false;
    private final Object mLock = new Object();
    private ClassicListener mListener;
    private Thread mThread;

    public ClassicThread(ClassicListener listener){
        mListener = listener;
        runThread();
    }

    private void runThread(){
        mThread = new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                synchronized (mLock) {
                    mHandler = new Handler();
                    mLooper = Looper.myLooper();
                    mLock.notifyAll();
                }
                Looper.loop();

            }
        });
        mThread.start();
        synchronized (mLock) {
            while(mHandler == null){
                try {
                    mLock.wait();
                } catch (InterruptedException e) {
                    e("can't wait for lock, interrupted");
                    e.printStackTrace();
                }
            }
        }

    }

    public void start(){
        l("starting");
        if(!mThread.isAlive())
            runThread();
        mHandler.post(mAudioLoop);
        mWorking = true;
        l("started");
    }

    public void stop(){
        mStop = true;
        l("mStopped: " + mStopped + ", mWorking: " + mWorking);
        synchronized (mLock) {
            while(!mStopped && mWorking){
                try {
                    mLock.wait();
                } catch (InterruptedException e) {
                    e("can't wait for lock");
                    e.printStackTrace();
                }
            }
        }
        mStopped = false;//clean up for next time
        mStop = false;
        mWorking = false;
        l("stopped");
    }

    public void quit(){
        l("quitting ...");
        if(mLooper ==  null)
            return;
        mLooper.quit();
        try {
            mThread.join();
            mWorking = false;
        } catch (InterruptedException e) {
            e("could not join Thread, interrupted");
            e.printStackTrace();
        }
        l("threads joined ...");
    }

    abstract protected boolean initLoop();

    abstract protected void finLoop();

    abstract protected boolean doLoop();

    private Runnable mAudioLoop = new Runnable() {
        public void run() {
            if(!initLoop()) return;
            for(;;){//this is the doLoop
                synchronized(mLock){
                    if(mStop){//break condition
                        mStopped = true;
                        finLoop();
                        mLock.notifyAll();
                        return;
                    }
                }
                if(!doLoop()) return;
                mListener.onDone();
            }
        }
    };

    protected void e(String msg){
        Log.e("ClassicThread", ">==< " + msg + " >==<");
    }
    protected void l(String msg){
        Log.d("ClassicThread", ">==< "+msg+" >==<");
    }

}
