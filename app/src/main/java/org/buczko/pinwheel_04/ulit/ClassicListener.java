package org.buczko.pinwheel_04.ulit;

/**
 * Created by marcin on 2015-05-19.
 */
public interface ClassicListener {

    public void onDone();
}
