package org.buczko.pinwheel_04.ulit;

import android.util.Log;

import org.buczko.pinwheel_04.AudioRecorder.PW_AudioSpeed;
import org.buczko.pinwheel_04.MagneticRecorder.PW_MagneticSpeed;
import org.buczko.pinwheel_04.PinWheelListener;

/**
 * Created by marcin on 2015-06-01.
 */
public abstract class PWSpeedThread implements Runnable{
    // ==== referancja do danych z sensorw
    protected double m_X[];   // odczyty z sensora magnetycznego wartosci pola
    protected long m_T[];     // czas w nanosekundach dla kazdego pomiaru
    protected int m_maxindex;         // wielkosc tablicy m_X i m_T
    protected PW_Record mPWRecord;

    //==== Table of speed history =========
    protected SpeedRecord mSpeedRecord;

    // ==== parametry pomocnicze =========
    protected int last_index;

    // ==== parametry
    protected int mdelay;         //czestotliwosc pomiarow w milisecundach
    protected PinWheelListener mListener;

    //STAEST
    protected boolean statusWorking;
    protected boolean statusAverage;

    // ===== wartosci zwracane ==================
    protected float average_speed;


    public PWSpeedThread(PW_Record PWRecord, SpeedRecord speedRecord, int delay) {
        mPWRecord = PWRecord;
        mSpeedRecord = speedRecord;
        m_X = PWRecord.getX();
        m_T = PWRecord.getT();
        m_maxindex = PWRecord.getmTableLength();
        mdelay = delay;
        mListener = null;
        statusWorking = false;
        statusAverage = false;
    }

    public void setmListener(PinWheelListener listener) {
        mListener = listener;
    }

    public void setStatusWorking(boolean statusWorking) {
        this.statusWorking = statusWorking;
    }

    public void setStatusAverage(boolean statusAverage) {
        this.statusAverage = statusAverage;
    }

    abstract protected float countPinwheelSpeed();

    abstract protected void doLoop();

    public void reset(int m_index){
        last_index = m_index;
        mSpeedRecord.reset();
        average_speed = 0;
    }

    @Override
    public void run() {
        d("started");
        while (statusWorking) {
            doLoop();
            mSpeedRecord.add(countPinwheelSpeed());
            if(statusAverage)
                mSpeedRecord.calculateAverageSpeed();
            if (this instanceof PW_MagneticSpeed)
                mListener.onMagneticSpeedResult();
            else if(this instanceof PW_AudioSpeed)
                mListener.onAudioSpeedResult();
            try {
                d("sleep");
                Thread.sleep(mdelay);
                d("wakeup");
            } catch (InterruptedException x) {
                throw new RuntimeException("interrupted", x);
            }
        }
        d("stoped");
    }

    protected void d(String msg){
        if(!Parameters.isDebug())
            return;
        if(this instanceof PW_MagneticSpeed)
            Log.d("PW_MagneticSpeed", "===> " + msg);
        else if(this instanceof PW_AudioSpeed)
            Log.d("PW_AudioSpeed", "===> " + msg);
        else
            Log.d("PWSpeedThread", "===> " + msg);
    }
}

