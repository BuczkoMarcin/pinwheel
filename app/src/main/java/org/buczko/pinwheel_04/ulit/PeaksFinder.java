package org.buczko.pinwheel_04.ulit;

import android.util.Log;

import java.util.Arrays;

/**
 * Created by marcin on 2015-05-31.
 */
public class PeaksFinder {

    // ==== referancja do danych z sensorow
     protected PW_Record mPWRecord;

    // === dane z sensora lokalnie przetwarzane
    protected double[] m_X;
    protected long[] m_T;
    protected int m_X_lenght;

    //=== parametry stale=======
    protected float mFactor_H;
    protected float mValid_H;

    //=== parametry ustawiane ====
    protected boolean mDebug = false;

    // ==== obliczone parametry ostaniego pomiaru ===========
    protected long    current_first_peak_time = 0;
    protected long    current_peak_time = 0;       // czas ostatniego peak'u
    protected double  current_peak_value;          // ostani peak
    protected int     current_peak_index;
    protected boolean last_min_present = false;   // został odczytany przynajmniej jeden min i jeden max
    protected boolean last_max_present = false;
    protected double  last_max_value;              // ostatni peak ktory by maxem
    protected double  last_min_value;              // ostatni peak ktory by minem
    protected int debug_peaks[];
    // private int debug_peaks_index = 0;

    public PeaksFinder(PW_Record PWRecord) {
        mPWRecord = PWRecord;
        m_X = PWRecord.getX();
        m_T = PWRecord.getT();
        m_X_lenght = PWRecord.getmTableLength();
        debug_peaks = PWRecord.getDebug_peaks();
        mFactor_H = 5;
        mValid_H = 3;
    }

    public void setmFactor_H(float mFactor_H) {
        this.mFactor_H = mFactor_H;
    }

    public void setmValid_H(float mValid_H) {
        this.mValid_H = mValid_H;
    }

    public void setDebug(boolean mDebug){
        this.mDebug = mDebug;
    }

    public long getCurrent_first_peak_time() {
        return current_first_peak_time;
    }

    public double getCurrent_peak_value() {
        return current_peak_value;
    }

    public long getCurrent_peak_time() {
        return current_peak_time;
    }

    public int getCurrent_peak_index() {
        return current_peak_index;
    }

    public boolean isLast_min_present() {
        return last_min_present;
    }

    public boolean isLast_max_present() {
        return last_max_present;
    }

    public double getLast_max_value() {
        return last_max_value;
    }

    public double getLast_min_value() {
        return last_min_value;
    }

    public double getLast_X_value() {
        return m_T[m_X_lenght-1];
    }

    public int[] getDebug_peaks() {
        return debug_peaks;
    }

    public int getDebug_peaks_index() {
        return mPWRecord.getDebug_peaks_index();
    }


    /*********************************************************************
     *  Liczy ilosc wystapien minunow i maximow w tablicy m_MagneticX w przedziale
     *  <start_index, stop_index>
     *
     *  Bierzemy wartosoc max i min z calego m_MagneticX. odejmujemy max od min i dzielimy przez factor
     *  lokalne maxima i minima musza miec wszystkie pubkty po lewej i prawej mniejsze/wieksze
     *  od tej wartosci aby byly uznane
     *  chodzi o zniwelowanie szumow
     **********************************************************************/
   /*
   * stop_index    koncowy index w probkach m_MAgneticX od ktorego zaczynamy liczyc
   * start_index   poczatkowy -  powinien byc last_index+1 (chyba, ze liczymy od poczatku)
   * m_maxidex     długość tablicy na której liczymy peaki (jest to talica MAgneticX)
   */


    protected int findPeaks(int start_index, int stop_index, int m_maxindex){
        int peaks = 0;
        int il = 0;
        int imax = 0;
        int imin = 0;
        int l_peaks[];
        int l_index_peaks = 0;
        current_first_peak_time = 0;
        double h;

        //mapXT(start_index, stop_index);
        h = calculateH();
        //Log.d("findPeaks","H: " + h);
        l_peaks = new int[m_X_lenght];

        if(h<mValid_H){
            if(mDebug)
                actualizeDebugPeaksArray(l_peaks,l_index_peaks,start_index,stop_index, m_maxindex);
            return 0;
        }

        for(int i=0;i< m_X_lenght;i++) {
            if      (m_X[i] > m_X[imax]) imax = i;
            else if (m_X[i] < m_X[imin]) imin = i;
            if ((m_X[imin] < m_X[il] - h) && (m_X[imin] < m_X[i] - h)) {
                peaks++;
                if(current_first_peak_time == 0)
                    current_first_peak_time = m_T[imin];
                current_peak_time  = m_T[imin];
                current_peak_value = m_X[imin];
                last_min_value     = m_X[imin];
                last_min_present   = true;
                if(imin + start_index < m_maxindex)
                    l_peaks[l_index_peaks] = imin + start_index;
                else
                    l_peaks[l_index_peaks] = imin + start_index - m_maxindex;
                current_peak_index = l_peaks[l_index_peaks];
                l_index_peaks++;
                il = imin;
                imax = i;
            } else if (m_X[imax] > m_X[il] + h && m_X[imax] > m_X[i] + h) {
                peaks++;
                if(current_first_peak_time == 0)
                    current_first_peak_time = m_T[imax];
                current_peak_time  = m_T[imax];
                current_peak_value = m_X[imax];
                last_max_value     = m_X[imax];
                last_max_present   = true;
                if(imax + start_index < m_maxindex)
                    l_peaks[l_index_peaks] = imax + start_index;
                else
                    l_peaks[l_index_peaks] = imax + start_index - m_maxindex;
                current_peak_index = l_peaks[l_index_peaks];
                l_index_peaks++;
                il = imax;
                imin = i;
            }
        }

       // Log.d("findPeaks()","l_peaks:" + Arrays.toString(l_peaks));
        if(mDebug)
            actualizeDebugPeaksArray(l_peaks,l_index_peaks,start_index,stop_index, m_maxindex);
        return peaks;
    }

    private void actualizeDebugPeaksArray(int l_peaks[], int l_index_peaks, int start_index, int stop_index, int m_maxindex){
        //Log.d("actualizeDebugPeaks","Doing");
        int new_peaks[] = new int[m_maxindex];
        int k = 0;
        if(l_index_peaks>0) {
            System.arraycopy(l_peaks, 0, new_peaks, 0, l_index_peaks);
            k = l_index_peaks;
        };
        int debug_peaks_index = mPWRecord.getDebug_peaks_index();
        for(int i=0;i<debug_peaks_index;i++){
            if(start_index < stop_index){
                if(debug_peaks[i]<start_index || debug_peaks[i]>stop_index) {
                    new_peaks[k] = debug_peaks[i];
                    k++;
                };
            }else{
                if(debug_peaks[i]<start_index && debug_peaks[i]>stop_index) {
                    new_peaks[k] = debug_peaks[i];
                    k++;
                };
            }
        }
        if(k > 0)
            Arrays.sort(new_peaks, 0, k);
        if(!mPWRecord.setDebug_peaks(new_peaks,k)) {
            System.err.print("actualizeDebugPeaksArray: cant change debug_peaks. Wrong parameters");
            Log.d("WARING", "actualizeDebugPeaksArray: cant change debug_peaks. Wrong parameters");
        }
        debug_peaks = mPWRecord.getDebug_peaks();
    }

    private double calculateH(){
        if(m_X_lenght <= 0) return 0;
        double max =  m_X[0];
        double min =  m_X[0];
        for (int i = 0; i < m_X_lenght; i++) {
            if (max < m_X[i]) max = m_X[i];
            if (min > m_X[i]) min = m_X[i];
        }
        return (max-min)/ mFactor_H;
    }

    public void reset(){
        //valid_time = false;
        //last_index = m_index;
        //last_index = 0;
        //old_peak_time = 0;
        mPWRecord.setDebug_peaks(debug_peaks, 0);
        last_min_present = false;
        last_max_present = false;
    }

}
