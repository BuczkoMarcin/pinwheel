package org.buczko.pinwheel_04.ulit;

import android.app.Activity;
import android.content.res.Configuration;
import android.util.Log;
import android.view.Display;

import org.buczko.pinwheel_04.AudioRecorder.AudioPeaksFinder;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioRecord;
import org.buczko.pinwheel_04.MagneticRecorder.MagneticPeaksFinder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by marcin on 2015-06-01.
 */
public class Parameters {

    private static boolean debug = false;

    public static final String Hz = "Hz";
    public static final String kmh = "Km/h";
    public static final String ms  = "m/s";
    //PinWheel
    public static enum PinwheelKind {MAGNETIC, AUDIO, NON};

    //SpeedREcord
    private static int SpeedRecordLenght = 50;
    private static int SpeedRecordGraphFactor = -10;
    private static int SpeedRecordMagneticGraphFactor = -10;
    private static int SpeedRecordAudioGraphFactor = -2;


    //PW_AudioRecord
    private static int mSampleRateInHz = 11025; //8000, 11025, 16000, 22050, 44100
    private static int mInterval = 20;
    private static int mMinPeaksInGroup = 3;
    private static int mMinJojnedPeaksInterval = 80;
    private static PW_AudioRecord mPW_AudioRecord;

    //AudioAdapter
    private static int AUDIO_BLOCK_SIZE = 16384;

    //AudioPeaksFinder
    private static float mAudioFactor_H = 2.7f;
    private static float mAudioValid_H = 1300;
    private static AudioPeaksFinder mAudioPeaksFinder;

    //MagneticPeaksFinder
    private static float mMagneticFactor_H = 5;
    private static float mMagneticValid_H = 3;
    private static MagneticPeaksFinder mMagneticPeaksFinder;

    public static int[] convertIntegersList(ArrayList<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        Log.d("convertIntegersList", ret.toString());
        return ret;
    }

    public static ArrayList<Integer> convertIntTable(int[] intTable){
        ArrayList<Integer>_arrayOfItegers = new ArrayList<>();
        for(int item: intTable){
            _arrayOfItegers.add(item);
        }
        Log.d("convertIntTable", _arrayOfItegers.toString());
        return _arrayOfItegers;
    }

    public static int getScreenOrientation(Activity a)
    {
        Display getOrient = a.getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if(getOrient.getWidth()==getOrient.getHeight()){
            orientation = Configuration.ORIENTATION_SQUARE;
        } else{
            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else {
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

    public static int getSpeedRecordMagneticGraphFactor() {
        return SpeedRecordMagneticGraphFactor;
    }

    public static void setSpeedRecordMagneticGraphFactor(int speedRecordMagneticGraphFactor) {
        SpeedRecordMagneticGraphFactor = speedRecordMagneticGraphFactor;
    }

    public static int getSpeedRecordAudioGraphFactor() {
        return SpeedRecordAudioGraphFactor;
    }

    public static void setSpeedRecordAudioGraphFactor(int speedRecordAudioGraphFactor) {
        SpeedRecordAudioGraphFactor = speedRecordAudioGraphFactor;
    }

    public static int getSpeedRecordGraphFactor() {
        return SpeedRecordGraphFactor;
    }

    public static void setSpeedRecordGraphFactor(int speedRecordGraphFactor) {
        SpeedRecordGraphFactor = speedRecordGraphFactor;
    }

    public static int getSpeedRecordLenght() {
        return SpeedRecordLenght;
    }

    public static void setSpeedRecordLenght(int speedRecordLenght) {
        SpeedRecordLenght = speedRecordLenght;
    }

    public static boolean isDebug() {
        return debug;
    }

    public static void setDebug(boolean debug) {
        Parameters.debug = debug;
    }

    public static void setmMagneticPeaksFinder(MagneticPeaksFinder mMagneticPeaksFinder) {
        Parameters.mMagneticPeaksFinder = mMagneticPeaksFinder;
    }

    public static float getmMagneticValid_H() {
        return mMagneticValid_H;
    }

    public static void setmMagneticValid_H(float mMagneticValid_H) {
        if(mMagneticPeaksFinder!=null)
            mMagneticPeaksFinder.setmValid_H(mMagneticValid_H);
        Parameters.mMagneticValid_H = mMagneticValid_H;
    }

    public static float getmMagneticFactor_H() {
        return mMagneticFactor_H;
    }

    public static void setmMagneticFactor_H(float mMagneticFactor_H) {
        if(mMagneticPeaksFinder!=null)
            mMagneticPeaksFinder.setmFactor_H(mMagneticFactor_H);
        Parameters.mMagneticFactor_H = mMagneticFactor_H;
    }

    public static void setmAudioPeaksFinder(AudioPeaksFinder mAudioPeaksFinder) {
        Parameters.mAudioPeaksFinder = mAudioPeaksFinder;
    }

    public static float getmAudioValid_H() {
        return mAudioValid_H;
    }

    public static void setmAudioValid_H(float mAudioValid_H) {
        if(mAudioPeaksFinder!=null)
            mAudioPeaksFinder.setmValid_H(mAudioValid_H);
        Parameters.mAudioValid_H = mAudioValid_H;
    }

    public static float getmAudioFactor_H() {
        return mAudioFactor_H;
    }

    public static void setmAudioFactor_H(float mAudioFactor_H) {
        if(mAudioPeaksFinder!=null)
            mAudioPeaksFinder.setmFactor_H(mAudioFactor_H);
        Parameters.mAudioFactor_H = mAudioFactor_H;
    }

    public static int getAUDIO_BLOCK_SIZE() {
        return AUDIO_BLOCK_SIZE;
    }

    public static void setAUDIO_BLOCK_SIZE(int AUDIO_BLOCK_SIZE) {
        Parameters.AUDIO_BLOCK_SIZE = AUDIO_BLOCK_SIZE;
    }

    public static void setPW_AudioRecord(PW_AudioRecord mPW_AudioRecord) {
        Parameters.mPW_AudioRecord = mPW_AudioRecord;
    }

    public static int getmMinJojnedPeaksInterval() {
        return mMinJojnedPeaksInterval;
    }

    public static void setmMinJojnedPeaksInterval(int mMinJojnedPeaksInterval) {
        Parameters.mMinJojnedPeaksInterval = mMinJojnedPeaksInterval;
    }

    public static int getSampleRateInHz() {
        return mSampleRateInHz;
    }

    public static void setSampleRateInHz(int mSampleRateInHz) {
        Parameters.mSampleRateInHz = mSampleRateInHz;
    }

    public static int getInterval() {
        return mInterval;
    }

    public static void setInterval(int mInterval) {
        Parameters.mInterval = mInterval;
    }

    public static int getMinPeaksInGroup() {
        return mMinPeaksInGroup;
    }

    public static void setMinPeaksInGroup(int mMinPeaksInGroup) {
        Parameters.mMinPeaksInGroup = mMinPeaksInGroup;
    }
}
