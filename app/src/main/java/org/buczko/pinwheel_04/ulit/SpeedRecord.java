package org.buczko.pinwheel_04.ulit;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by marcin on 17.05.15.
 */
public class SpeedRecord implements Parcelable {

    private float[] m_out;          // tablica z pomiarem obrotow co staly przedzial czasowy np co 1sek.
    private int m_indexout;
    private final int m_maxindexout;
    private boolean first_out;
    private float mAverage_speed;
    private float mAverage_line;


    public SpeedRecord(int tableLenght) {
        m_maxindexout = tableLenght;
        m_indexout = 0;
        m_out = new float[tableLenght];
        first_out = false;
        mAverage_line = 0;
        mAverage_speed = 0;
    }

    public float[] getM_out() {
        return m_out;
    }

    public void add(float speed){
        if(Float.isNaN(speed))
            speed = 0f;
        m_out[m_indexout] = Parameters.getSpeedRecordGraphFactor() * speed;
        m_indexout = (m_indexout + 1 == m_maxindexout) ? 0 : m_indexout + 1;
        if(m_indexout == 0)
            first_out = true;
    }

    public int getM_indexout() {
        return m_indexout;
    }

    public boolean isFirst_out() {
        return first_out;
    }

    public int getM_maxindexout() {
        return m_maxindexout;
    }

    public int getCurrentIndex(){
        if(m_indexout == 0)
            return m_maxindexout -1;
        return m_indexout - 1;
    }

    public float getmAverage_speed() {
        return mAverage_speed;
    }

    public float getmAverage_line(){
        return mAverage_line;
    }

    public float getCurrentSpeed(){
        if(m_indexout == 0)
            return m_out[m_maxindexout-1]/Parameters.getSpeedRecordGraphFactor();
        return m_out[m_indexout-1]/Parameters.getSpeedRecordGraphFactor();
    }

    public void reset(){
        for(int i = 0; i<m_maxindexout; i++)
            m_out[i] = 0;
        m_indexout = 0;
        first_out = false;
        mAverage_speed = 0;
        mAverage_line = 0;
    }

    public void calculateAverageSpeed(){
        mAverage_speed = 0;
        Log.d("calculateAverageSpeed","first_out" + first_out);
        if(!first_out && m_indexout > 0) {
            for (int i = 0; i < m_indexout; i++) {
                mAverage_speed += m_out[i];
            }
            mAverage_speed /= m_indexout;
        }else{
            for (int i = 0; i < m_maxindexout; i++) {
                mAverage_speed += m_out[i];
            }
            mAverage_speed /= m_maxindexout;
        }
        mAverage_line = mAverage_speed;
        mAverage_speed /= Parameters.getSpeedRecordGraphFactor();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloatArray(this.m_out);
        dest.writeInt(this.m_indexout);
        dest.writeInt(this.m_maxindexout);
        dest.writeByte(first_out ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.mAverage_speed);
        dest.writeFloat(this.mAverage_line);
    }

    private SpeedRecord(Parcel in) {
        this.m_out = in.createFloatArray();
        this.m_indexout = in.readInt();
        this.m_maxindexout = in.readInt();
        this.first_out = in.readByte() != 0;
        this.mAverage_speed = in.readFloat();
        this.mAverage_line = in.readFloat();
    }

    public static final Parcelable.Creator<SpeedRecord> CREATOR = new Parcelable.Creator<SpeedRecord>() {
        public SpeedRecord createFromParcel(Parcel source) {
            return new SpeedRecord(source);
        }

        public SpeedRecord[] newArray(int size) {
            return new SpeedRecord[size];
        }
    };
}
