package org.buczko.pinwheel_04.AudioRecorder;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.ClassicListener;
import org.buczko.pinwheel_04.ulit.ClassicThread;




public class AudioAdapter extends ClassicThread {


    private short[] mAudioShortBuffer;
   //private float [] mProjection;

    private AudioRecord mRec;

    public AudioAdapter(ClassicListener listener ) {
        super(listener);
        mAudioShortBuffer = new short[Parameters.getAUDIO_BLOCK_SIZE()];
    }

    public int getSampleRateInHz() {
        return Parameters.getSampleRateInHz();
    }


    public void copyBuffer(double[] samples){
        int  BLOCK_SIZE = Parameters.getAUDIO_BLOCK_SIZE();
        for(int i=0; i < BLOCK_SIZE ; i++){
            samples[i] = mAudioShortBuffer[i];
        }
    }

    @Override
    protected boolean initLoop() {
        l("Block size: "+ Parameters.getAUDIO_BLOCK_SIZE());
        //ByteBuffer audioBuffer = ByteBuffer.allocateDirect(BLOCK_SIZE);
        mRec = new AudioRecord(
                MediaRecorder.AudioSource.MIC,
                Parameters.getSampleRateInHz(),
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                16* Parameters.getAUDIO_BLOCK_SIZE()
        );
        if(mRec.getState() == AudioRecord.STATE_UNINITIALIZED){
            e("cannot initialzie audio");
            return false;
        }
        mRec.startRecording();
        l("start recording");
        return true;

    }

    @Override
    protected void finLoop() {
        mRec.stop();
    }

    @Override
    protected boolean doLoop() {
        //l("Looping!");
        if( mRec.read(mAudioShortBuffer, 0, Parameters.getAUDIO_BLOCK_SIZE()) == AudioRecord.ERROR_BAD_VALUE){
            e("read: bad value given");
            mRec.stop();
            return false;
        }
        return true;

    }

    @Override
    public void quit() {
        super.quit();
        if(mRec!=null)
            mRec.release();
    }

    @Override
    public void stop() {
        d("stoping");
        super.stop();
        d("mic releasing");
        if(mRec!=null)
            mRec.release();
        d("mic released");
    }


    protected void d(String msg){
        if(Parameters.isDebug())
            Log.d("AudioAdapter", "===> " + msg);
    }
}
