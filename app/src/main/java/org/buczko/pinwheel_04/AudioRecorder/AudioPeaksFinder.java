package org.buczko.pinwheel_04.AudioRecorder;

import android.util.Log;

import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.PeaksFinder;

/**
 * Created by marcin on 2015-05-31.
 */
public class AudioPeaksFinder extends PeaksFinder {
    private PW_AudioRecord mPWAudioRecord;

    public AudioPeaksFinder(PW_AudioRecord pw_audioRecord) {
        super(pw_audioRecord);
        mPWAudioRecord = (PW_AudioRecord)mPWRecord;
        Parameters.setmAudioPeaksFinder(this);
        mFactor_H = Parameters.getmAudioFactor_H();
        mValid_H = Parameters.getmAudioValid_H();
        mDebug = true;
    }

    public int findPeaks() {
        mDebug = true;
        //Log.d("findPeaks","mFactor_H: "+ mFactor_H);
        super.findPeaks(0, m_X_lenght - 1, m_X_lenght);
        int peaks = mPWAudioRecord.joinCloseDebugPeaks();
        return peaks;
    }
}
