package org.buczko.pinwheel_04.AudioRecorder;

import android.util.Log;

import org.buczko.pinwheel_04.ulit.PWSpeedThread;
import org.buczko.pinwheel_04.ulit.SpeedRecord;

/**
 * Created by marcin on 2015-06-01.
 */


public class PW_AudioSpeed extends PWSpeedThread implements Runnable {

    public int mRunCounter;
    AudioPeaksFinder mAudioPeaksFinder;

    public PW_AudioSpeed(PW_AudioRecord pw_audioRecord, SpeedRecord speedRecord, int delay) {
        super(pw_audioRecord, speedRecord, delay);
        mAudioPeaksFinder = new AudioPeaksFinder(pw_audioRecord);
        mAudioPeaksFinder.setDebug(true);

    }

    @Override
    protected float countPinwheelSpeed() {
        float peaks;
        long time_interval;
        d("countPinwheelSpeed");
        synchronized(this) {
            while(!((PW_AudioRecord)mPWRecord).isFreshData()) {
                try {
                    d("wait");
                    wait();
                    d("wakeup");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //Log.d("countPinwheelSpeed", "countPinwheelSpeed");
            peaks = mAudioPeaksFinder.findPeaks();
            time_interval = m_T[((PW_AudioRecord)mPWRecord).getmMedianInterval()];
            ((PW_AudioRecord)mPWRecord).setFreshData(false);
            notify();;
        }
        if(peaks<2)
            return 0;
        return  1000000000f / (float)time_interval;
       // long time_interval = mAudioPeaksFinder.getCurrent_peak_time() - mAudioPeaksFinder.getCurrent_first_peak_time();
      //  return peaks / ((float) time_interval / 1000000000);

    }

    @Override
    protected void doLoop() {

    }

}
