package org.buczko.pinwheel_04.AudioRecorder;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.PW_Record;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by marcin on 2015-05-31.
 */
public class PW_AudioRecord extends PW_Record implements Parcelable {

    private ArrayList<Integer> mJoinedPeaks;
    private ArrayList<Integer> mMedianPeaks;
    private ArrayList<Integer> mJoinedIntervals;
    private int mMedianInterval;
    private boolean mFreshData;

    public PW_AudioRecord() {
        super(Parameters.getAUDIO_BLOCK_SIZE());
        Parameters.setPW_AudioRecord(this);
        mJoinedPeaks = new ArrayList<>();
        mJoinedIntervals = new ArrayList<>();
        mMedianPeaks = new ArrayList<>();
        mFreshData = false;
        fillTime();
    }

    public int getmMedianInterval() {
        return mMedianInterval;
    }

    public boolean isFreshData() {
        return mFreshData;
    }

    public void setFreshData(boolean mFreshData) {
        this.mFreshData = mFreshData;
    }

    public List<Integer> getJoinedPeaks() {
        return mJoinedPeaks;
    }

    public int getFirstJoinedPeak(){
        if(mJoinedPeaks.size() > 0)
            return mJoinedPeaks.get(0);
        return 0;
    }

    public int getLasttJoinedPeak(){
        if(mJoinedPeaks.size() > 0)
            return mJoinedPeaks.get(mJoinedPeaks.size()-1);
        return 0;
    }

    public int JoinedPeaks(int index) {
        if(index<mJoinedPeaks.size() && index >= 0)
            return mJoinedPeaks.get(index);
        return 0;
    }

    public int[] getJoinedPeaksInt() {
        int[] joinedPeaks = new int[ mJoinedPeaks.size()];
        int i =0;
        for(Integer item: mJoinedPeaks){
            joinedPeaks[i] = item;
            i++;
        }
        return joinedPeaks;
    }

    public int[] getMedianPeakInt() {
        int[] medianPeaks = new int[ mMedianPeaks.size()];
        int i =0;
        for(Integer item: mMedianPeaks){
            medianPeaks[i] = item;
            i++;
        }
        return medianPeaks;
    }

    public void setMinPeaksInGroup(int mMinPeaksInGroup) {
        Parameters.setMinPeaksInGroup(mMinPeaksInGroup);
    }

    public void setInterval(int mInterval) {
        Parameters.setInterval(mInterval);
    }

    public void fillFloatX(int divisor){
        for(int i=0; i<mTableLength; i++){
            mFloatX[i] = (float)mX[i]/divisor;
        }
    }

    private void fillTime(){
       int SampleRateInHz = Parameters.getSampleRateInHz();
       if(SampleRateInHz == 0)
            return;
       long nanoTimeSample = 1000000000/(long) SampleRateInHz;
       long nanoTime = 0;
       for(int i=0; i< mTableLength; i++) {
            mT[i] = nanoTime;
            nanoTime = nanoTime + nanoTimeSample;
       };
   }

    public int joinCloseDebugPeaks(){
        int interval = Parameters.getInterval();
        int minPeaksInGroup = Parameters.getMinPeaksInGroup();
        int group_sum = 0;
        int peaks_in_group = 0;
        boolean isgroup = false;
        mJoinedPeaks.clear();
        mMedianPeaks.clear();
        mJoinedIntervals.clear();
        mMedianInterval = 0;
        for(int i=1; i < debug_peaks_index; i++){
            if(debug_peaks[i] - debug_peaks[i-1] <= interval){
                if(isgroup) {
                    peaks_in_group ++;
                    group_sum += debug_peaks[i];
                }else{
                    isgroup = true;
                    peaks_in_group = 2;
                    group_sum = debug_peaks[i] + debug_peaks[i-1];
                }
            }else{
                if(isgroup){
                    if(peaks_in_group >= minPeaksInGroup)
                        mJoinedPeaks.add(group_sum/peaks_in_group);
                    isgroup = false;
                }
            }
        }
        if(mJoinedPeaks.size() < 2) {
            return mJoinedPeaks.size();
        };

        calcMedianInterval();
        if(Parameters.isDebug())
            calcMedianPeaks();
        //Log.d("joinCloseDebugPeaks","mJoinedPeaks => " + mJoinedPeaks);
        //Log.d("joinCloseDebugPeaks","mJoinedIntervals => " + mJoinedIntervals);
        //Log.d("joinCloseDebugPeaks","mMedianPeaks => " + mMedianPeaks);
        //return mJoinedPeaks.size();
        if(mMedianInterval==0)
            return 0;
        return mTableLength/ mMedianInterval;
    }

    private void calcMedianPeaks(){
        if(mMedianInterval<=0)
            return;
        int l_peaks = mTableLength/ mMedianInterval;
        mMedianPeaks.clear();
        int first_index = (mTableLength - l_peaks*(int)(mTableLength/l_peaks) + l_peaks)/2;
        for(int i = first_index; i<mTableLength; i+= mMedianInterval)
            mMedianPeaks.add(i);
    }

    private void calcMedianInterval(){
        mMedianInterval = 0;
        int local_interval = 0;
        mJoinedIntervals.clear();
        for(int i=1; i<mJoinedPeaks.size();i++){
            local_interval = mJoinedPeaks.get(i) - mJoinedPeaks.get(i-1);
            if(local_interval > Parameters.getmMinJojnedPeaksInterval())
                mJoinedIntervals.add(local_interval);
        }
        Collections.sort(mJoinedIntervals);
        int l_Intervals_size = mJoinedIntervals.size();
        if(l_Intervals_size == 0)
            return;
        if(l_Intervals_size >= 4) {
            int l_first = l_Intervals_size/4;
            int l_last = 3*l_Intervals_size/4;
            for (int i = l_first  ; i < l_last; i++) {
                mMedianInterval += mJoinedIntervals.get(i);
            }
            mMedianInterval /= (l_last - l_first);
           // Log.d("joinCloseDebugPeaks", "l_first:" + l_first + " l_last: " + l_last + "l_averageInterval:" + l_averageInterval);
        }else{
            for(int val: mJoinedIntervals)
                mMedianInterval += val;
            mMedianInterval /= l_Intervals_size;
        }
    }

    //=============== Parcelable ===================================================================


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.mJoinedPeaks);
        //dest.writeIntArray(Parameters.convertIntegersList(mJoinedPeaks));
        dest.writeList(this.mMedianPeaks);
        //dest.writeIntArray(Parameters.convertIntegersList(mMedianPeaks));
        dest.writeList(this.mJoinedIntervals);
        dest.writeIntArray(Parameters.convertIntegersList(mJoinedIntervals));
        dest.writeInt(this.mMedianInterval);
        dest.writeByte(mFreshData ? (byte) 1 : (byte) 0);
        dest.writeDoubleArray(this.mX);
        dest.writeLongArray(this.mT);
        dest.writeFloatArray(this.mFloatX);
        dest.writeInt(this.mTableLength);
        dest.writeInt(this.mNextIndex);
        dest.writeIntArray(this.debug_peaks);
        dest.writeInt(this.debug_peaks_index);
    }

    private PW_AudioRecord(Parcel in) {
        this.mJoinedPeaks = (ArrayList<Integer>) in.readArrayList(Integer.class.getClassLoader());
        //this.mJoinedPeaks = Parameters.convertIntTable(in.createIntArray());
        this.mMedianPeaks = (ArrayList<Integer>) in.readArrayList(Integer.class.getClassLoader());
        //this.mMedianPeaks = Parameters.convertIntTable(in.createIntArray());
        this.mJoinedIntervals = (ArrayList<Integer>) in.readArrayList(Integer.class.getClassLoader());
        this.mJoinedIntervals = Parameters.convertIntTable(in.createIntArray());
        this.mMedianInterval = in.readInt();
        this.mFreshData = in.readByte() != 0;
        this.mX = in.createDoubleArray();
        this.mT = in.createLongArray();
        this.mFloatX = in.createFloatArray();
        this.mTableLength = in.readInt();
        this.mNextIndex = in.readInt();
        this.debug_peaks = in.createIntArray();
        this.debug_peaks_index = in.readInt();
    }

    public static final Parcelable.Creator<PW_AudioRecord> CREATOR = new Parcelable.Creator<PW_AudioRecord>() {
        public PW_AudioRecord createFromParcel(Parcel source) {
            return new PW_AudioRecord(source);
        }

        public PW_AudioRecord[] newArray(int size) {
            return new PW_AudioRecord[size];
        }
    };
}
