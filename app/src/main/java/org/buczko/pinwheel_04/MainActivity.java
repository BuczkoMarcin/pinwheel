package org.buczko.pinwheel_04;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.buczko.pinwheel_04.AudioRecorder.AudioAdapter;
import org.buczko.pinwheel_04.AudioRecorder.PW_AudioRecord;
import org.buczko.pinwheel_04.MagneticRecorder.PW_MagneticRecord;
import org.buczko.pinwheel_04.ulit.Parameters;
import org.buczko.pinwheel_04.ulit.SpeedRecord;
import org.buczko.pinwheel_04.ulit.GraphView;

import java.util.Locale;


public class MainActivity extends Activity implements SensorEventListener, PinWheelListener {

    //STATES
    private boolean mStatusWorking = false;
    private boolean mStatusAverage = false;
    private boolean mStatusSpeed_graph = true;

    private SensorManager m_SensorManager;
    private GraphView mGraphView;
    private TextView textSpeed_Hz;
    private TextView textStatus;

    Button btnStart;
    Button clrBtn;
    Button avgBtn;
    Button graphbtn;

    private PW_MagneticRecord mPWMagneticRecord;
    private PW_AudioRecord mPWAudioRecord;
    private SpeedRecord mSpeedRecord;
    private PinWheel mPinWheel;

    private OnMagneticSpeedResult mOnMagneticSpeedResult;
    private OnAudioSpeedResult mOnAudioSpeedResult;
    private SpeedTestDebug m_SpdDebug = new SpeedTestDebug();
    private AudioAdapter mAudioAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        m_SensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        //===== UI ==========================================
        mGraphView = (GraphView)findViewById(R.id.WindGraph);
        textSpeed_Hz = (TextView)findViewById(R.id.textSpeed_Hz);
        textStatus = (TextView)findViewById(R.id.textStatus);
        showStatus();
        organizeScreen();
        //===== RECORDS =========================================
        //ustawiamy dlugosc tablicy z czytywanymi danymiz sensora pola magnetycznego
        mPWMagneticRecord = new PW_MagneticRecord(1024);
        //ustawiamy dlugosc tablicy z odczytamipredkosci i wspolczynnik skali potrzebny dorysowaniua grafu
        mSpeedRecord = new SpeedRecord(Parameters.getSpeedRecordLenght());
        //Instancja dancyh wejsciowych microfonu
        mPWAudioRecord = new PW_AudioRecord();
        // ====== PINWHEEL ======================================
        //wskazania na recordy i delay w milisecundach pco jaki czas beda robione obliczeni predkosci
        mPinWheel = new PinWheel(mPWMagneticRecord, mPWAudioRecord, mSpeedRecord, 1000);
        mPinWheel.setListener(this);
        mPinWheel.setAudio();
        Parameters.setSpeedRecordGraphFactor(-2);
        //======= RUNNABLES ======================================
        mOnMagneticSpeedResult = new OnMagneticSpeedResult();
        mOnAudioSpeedResult = new  OnAudioSpeedResult();
    }

    private void organizeScreen(){
        final RelativeLayout m_RL = (RelativeLayout)findViewById(R.id.relativeLayout);
        m_RL.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout()
            {
                // gets called after layout has been done but before display.
                m_RL.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                btnStart = (Button)findViewById(R.id.StartButton);
                clrBtn = (Button)findViewById(R.id.ClrButton);
                avgBtn = (Button)findViewById(R.id.AvgButton);
                graphbtn = (Button)findViewById(R.id.GraphButton);



                int widthRL = m_RL.getWidth();
                int heightRL = m_RL.getHeight();
                Log.d("onCreate","widthRL " + widthRL);

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnStart.getLayoutParams();
                params.width  = widthRL/4;
                params.height = widthRL/4;
                btnStart.setLayoutParams(params);

                params = (RelativeLayout.LayoutParams) clrBtn.getLayoutParams();
                params.width  = widthRL/4;
                params.height = widthRL/4;
                clrBtn.setLayoutParams(params);

                params = (RelativeLayout.LayoutParams) avgBtn.getLayoutParams();
                params.width  = widthRL/4;
                params.height = widthRL/4;
                avgBtn.setLayoutParams(params);

                params = (RelativeLayout.LayoutParams) graphbtn.getLayoutParams();
                params.width  = widthRL/4;
                params.height = widthRL/4;
                graphbtn.setLayoutParams(params);


                final RelativeLayout m_mainRL = (RelativeLayout)findViewById(R.id.mainlayout);
                params = (RelativeLayout.LayoutParams) mGraphView.getLayoutParams();
                params.height = (m_mainRL.getHeight() - m_mainRL.getPaddingBottom() - m_mainRL.getPaddingTop())/2;
                mGraphView.setLayoutParams(params);

                clearData();
                // get width and height
            }
        });
    }

    private void showStatus(){
        if(textStatus==null) return;
        String statstxt = new String();
        if(mStatusWorking)
            statstxt += "WORKING";
        else
            statstxt += "STOPED";

        if(mStatusAverage)
            statstxt += "/A";

        if(mStatusSpeed_graph)
            statstxt += "/SPD";
        else
            statstxt += "/MGT";
        textStatus.setText(statstxt);
    }

    private void showSpeedHz(){
        if(mStatusAverage) {
            textSpeed_Hz.setText(String.format(Locale.US,"%.1f", mSpeedRecord.getmAverage_speed()) + " Hz");
        }else
            textSpeed_Hz.setText(String.format(Locale.US,"%.1f", mSpeedRecord.getCurrentSpeed()) + " Hz");
    }

    public void stop(){
        mStatusWorking=false;
        mPinWheel.stop();
        btnStart.setText("START");
    }

    public void start(){
        mStatusWorking=true;
        mPinWheel.start();
        btnStart.setText("STOP");
    }

    //============================ Sensor Listener =================================================
    protected void onResume() {
        super.onResume();
        m_SensorManager.registerListener(this, m_SensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
        showStatus();
    }

    protected void onPause() {
        super.onPause();
        m_SensorManager.unregisterListener(this);
        stop();
        showStatus();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(mStatusWorking && mPinWheel.isMagnetic()) {
            mPWMagneticRecord.add(event);
        };
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //===================== on PinWheel listener ====================================================

    @Override
    public void onMagneticSpeedResult() {
        runOnUiThread(mOnMagneticSpeedResult);
    }

    @Override
    public void onAudioSpeedResult() {
        runOnUiThread(mOnAudioSpeedResult);
    };

    private class OnMagneticSpeedResult implements Runnable {

        @Override
        public void run() {
            showSpeedHz();
            if(mStatusSpeed_graph)
                showSpeedGraph();
            else
                showMagneticGraph();
        }
    }

    private class OnAudioSpeedResult implements Runnable {

        @Override
        public void run() {
            showSpeedHz();
            if(mStatusSpeed_graph)
                showSpeedGraph();
            else
                showAudioGrap();
        }
    }

    //===================== Buttons OnClick Listener ===============================================
    public void clearData(){
        mSpeedRecord.reset();
        mPWMagneticRecord.reset();

        if(mStatusSpeed_graph) {
            setSpeedGraph();
            mGraphView.setRysikIndex(0);
        }else {
            if(mPinWheel.isMagnetic())
                setMagneticGraph();
            else if(mPinWheel.isAudio())
                setAudioGrap();
        };
        mGraphView.invalidate();
    }

    public void onStartButtonClick(){
        if(mStatusWorking){
            stop();
        }else{
            start();
        };
    }

    public void onGraphButtonClick(){
        if(mStatusSpeed_graph){
            mStatusSpeed_graph = false;
            if(mPinWheel.isMagnetic()) {
                setMagneticGraph();
                graphbtn.setText("MGNT.");
            }else if(mPinWheel.isAudio()){
                setAudioGrap();
                graphbtn.setText("AUDIO");
            }
        }else{
            mStatusSpeed_graph = true;
            setSpeedGraph();
            graphbtn.setText("SPEED");

        }
        mGraphView.autoScaleX();
        mGraphView.invalidate();
    }

    public void onAverageButtonClick(){
        if(mStatusAverage){
            mStatusAverage = false;
            mPinWheel.setAverageSpeed(false);
            mGraphView.setAvgLine(0);
            avgBtn.setText("AVG.\nOFF");
        }else{
            mStatusAverage = true;
            mPinWheel.setAverageSpeed(true);
            mSpeedRecord.calculateAverageSpeed();
            mGraphView.setAvgLine(mSpeedRecord.getmAverage_speed());
            avgBtn.setText("AVG.\nON");
        }
        mGraphView.invalidate();
    }

    public void onClick(View v) {

        //v.playSoundEffect(SoundEffectConstants.CLICK);
        switch (v.getId()){
            case R.id.StartButton: {
                onStartButtonClick();
            }break;

            case R.id.ClrButton: {
                clearData();
            }break;

            case R.id.AvgButton: {
                onAverageButtonClick();
            }break;

            case R.id.GraphButton: {
                onGraphButtonClick();
            }break;
        }
        showStatus();
    }

    //===================== GRAPHS =================================================================

    private void setMagneticGraph(){
        mGraphView.setMarkersArray(mPWMagneticRecord.getDebug_peaks());
        mGraphView.setGraph_byref(mPWMagneticRecord.getmFloatX());
        mGraphView.mRysikGraph = false;
        mGraphView.mOnlyPositiveGraph = false;
        mGraphView.setShadow(mPWMagneticRecord.getStart_i(), mPWMagneticRecord.getStop_i());
        mGraphView.setMarkers(50);
    }

    private void setSpeedGraph(){
        mGraphView.setMarkersArray(null);
        mGraphView.setGraph_byref(mSpeedRecord.getM_out());
        mGraphView.mRysikGraph = true;
        mGraphView.mOnlyPositiveGraph = true;
        mGraphView.setRysikIndex(mSpeedRecord.getCurrentIndex());
        mGraphView.setShadow(0, 0);
        mGraphView.setMarkers(0);
    }

    private void setAudioGrap(){
        mGraphView.setMarkersArray(mPWAudioRecord.getDebug_peaks());
        mGraphView.setGraph_byref(mPWAudioRecord.getmFloatX());
        mGraphView.mRysikGraph = false;
        mGraphView.mOnlyPositiveGraph = false;
        mGraphView.setShadow(0,0);
        mGraphView.setMarkers(100);
    }

    private void showMagneticGraph(){
        mGraphView.setShadow(mPWMagneticRecord.getStart_i(), mPWMagneticRecord.getStop_i());
        mGraphView.setMarkersArray(mPWMagneticRecord.getDebug_peaks());
        mGraphView.invalidate();
    }

    private void showSpeedGraph(){
        if(mStatusAverage)
            mGraphView.setAvgLine(mSpeedRecord.getmAverage_line());
        mGraphView.setRysikIndex(mSpeedRecord.getCurrentIndex());
        mGraphView.invalidate();

    }

    private void showAudioGrap(){
        mPWAudioRecord.fillFloatX(150);
        mGraphView.setMarkersArray(mPWAudioRecord.getMedianPeakInt());
        mGraphView.setMarkersArray2(mPWAudioRecord.getJoinedPeaksInt());
        mGraphView.invalidate();
    }

    // ===================== SPEED DEBUG CLASS =====================================================

    protected class SpeedTestDebug implements Runnable{
        private int mDebugRuns = 0;
        private float mDebugHz = 2f;
        private int mDebugDelay = 10;
        @Override
        public void run() {
            long time;
            while(true) {
                if (mStatusWorking) {
                    mDebugRuns = (mDebugRuns == mPWMagneticRecord.getmTableLength()-1 * 4) ? 0 : mDebugRuns + 1;
                    if (mDebugRuns == 0)
                        mDebugHz = (mDebugHz > 100) ? 0.1f : mDebugHz * 2;
                        time = System.nanoTime();
                        mPWMagneticRecord.add(30 * Math.sin((double) (mDebugHz * (6.28318d * time / 1000000000d))),
                                            System.nanoTime());
                }
                try {
                    Thread.sleep(mDebugDelay);
                } catch (InterruptedException x) {
                    throw new RuntimeException("interrupted", x);
                }
            }
        }
    }

}
