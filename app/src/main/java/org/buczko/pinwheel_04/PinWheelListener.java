package org.buczko.pinwheel_04;

import android.os.Handler;

/**
 * Created by marcin on 16.05.15.
 */
public interface PinWheelListener {

    public void onMagneticSpeedResult();

    public void onAudioSpeedResult();
}
